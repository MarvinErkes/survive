/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.ranking;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.util.RObjectRestore;

/**
 *
 * @author Rince
 */
public class RRankingSign extends RRankingObject
{
    
    private static List<RRankingSign> signs = new ArrayList<>();
    private static List<String> lines = new ArrayList<String>();
    
    @SuppressWarnings("unchecked")
	public RRankingSign(Location loc, int ranking_pos)
    {
        super(loc, ranking_pos);
        signs.add(this);
        
        setLines((List<String>)Survive.getInstance().convertPath(RConfigPath.RANKING_SINGS_LINES));
    }

    @Override
    public void update(RObjectRestore restore)
    {
        Location loc = getLocation();
        if(loc.getBlock().getState() instanceof Sign){
            Sign s = (Sign) loc.getBlock().getState();
            s.setLine(0, Survive.getInstance().getUserAPI().getUser(UUID.fromString(restore.getObject("uuid").toString())).getName());
            for(int i = 0; i < lines.size();i++){
                String l = replaceLine(restore, lines.get(i));
                s.setLine(i, l);
                s.update(true);
            }
        } else {
            loc.getBlock().setType(Material.SIGN_POST);
            update(restore);
        }
    }
    
    private String replaceLine(RObjectRestore restore,String line){
        line = line.replace("%kills%",Integer.toString((int)restore.getObject("kills")));
        line = line.replace("%deaths%", Integer.toString((int) restore.getObject("deaths")));
        line = line.replace("%points%", Integer.toString((int) restore.getObject("points")));
        line = line.replace("&", "§");
        if(line.contains("%name%")){
            line = line.replace("%name%", restore.getObject("name").toString());
        }
        
        return line;
    }
    public void remove(){
        signs.remove(this);
        objects.remove(this);
    }
    
    public static void setLines(List<String> lines_){
        if(lines_.size() > 4){
            throw new ArrayIndexOutOfBoundsException("List not valid!");
        }
        lines = lines_;
    }
    public static List<String> getLines(){
        return lines;
    }
    public static RRankingSign[] getRankingSigns(){
        RRankingSign[] list = new RRankingSign[signs.size()];
        return signs.toArray(list);
    }
    
    public static RRankingSign getbyRank(int rank){
        for(RRankingSign sign : signs){
            if(sign.getRankingPos() == rank){
                return sign;
            }
        }
        return null;
    }
}
