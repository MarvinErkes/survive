/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.ranking;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Skull;

import io.rince.survive.util.RObjectRestore;

/**
 *
 * @author Rince
 */
public class RRankingHead extends RRankingObject
{

    private static List<RRankingHead> heads = new ArrayList<>();
    
    public RRankingHead(Location loc, int ranking_pos)
    {
        super(loc, ranking_pos);
        heads.add(this);
    }

    @Override
    public void update(RObjectRestore restore)
    {
        Location loc = getLocation();
        
        loc.getBlock().setType(Material.SKULL);
        Skull s = (Skull) loc.getBlock().getState();
        s.setOwner(restore.getObject("name").toString());
        s.update(true);
    }
    public void remove(){
        heads.remove(this);
        objects.remove(this);
    }
    
    public static RRankingHead[] getRankingHeads(){
        RRankingHead[] list = new RRankingHead[heads.size()];
        return heads.toArray(list);
    }
    public static RRankingHead getbyRank(int rank){
        for(RRankingHead head : heads){
            if(head.getRankingPos() == rank){
                return head;
            }
        }
        return null;
    }
}
