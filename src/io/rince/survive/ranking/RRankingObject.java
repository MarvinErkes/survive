/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.ranking;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RStatsType;
import io.rince.survive.manager.RStatsConnection;
import io.rince.survive.userdatabase.UserProfile;
import io.rince.survive.util.RObjectRestore;

public abstract class RRankingObject
{
    
    private Location loc;
    private int ranking;
    
    protected static List<RRankingObject> objects = new ArrayList<>();
    
    public RRankingObject(Location loc,int ranking_pos){
        this.loc = loc;
        this.ranking = ranking_pos;
        objects.add(this);
    }
    
    public Location getLocation(){
        return loc;
    }
    public int getRankingPos(){
        return ranking;
    }
    public void setLocation(Location loc){
        this.loc.getBlock().setType(Material.AIR);
        this.loc = loc;
    }
    
    
    public abstract void update(RObjectRestore restore);
    
    
    public static void updateAll(List<RRankingObject> objects){
        RStatsConnection stats = Survive.getInstance().getStatsConnection();
        RRankingObject[] list = sortByRank(objects);
        if(list.length == 0){
            return;
        }
        
        String order = Survive.getInstance().convertPath(RConfigPath.RANKING_ORDER_TYPE).toString();
        RStatsType type = RStatsType.getBy(order);
        
        List<UserProfile> players = stats.getTopPlayer(type, list[0].getRankingPos());
        Bukkit.broadcastMessage(list[0].getRankingPos() + "");
        int current_head = 0;
        int current_sign = 0;
        int use = 0;
        
        for(RRankingObject ob : objects){
            if(ob instanceof RRankingHead){
                current_head = ob.getRankingPos()-1;
            }
            if(ob instanceof RRankingSign){
                current_sign = ob.getRankingPos()-1;
            }
            use = (ob instanceof RRankingHead ? current_head : current_sign);
            
            RObjectRestore restore = new RObjectRestore();
            restore.addObject("uuid", players.get(use).getUUID());
            restore.addObject("name", players.get(use).getName());
            restore.addObject("kills", stats.getStats(players.get(use).getUUID(), RStatsType.KILL));
            restore.addObject("deaths", stats.getStats(players.get(use).getUUID(), RStatsType.DEATH));
            restore.addObject("points", stats.getStats(players.get(use).getUUID(), RStatsType.POINT));
            
            
            ob.update(restore);
            
        }
    }
    /*public static void updateAll(){
        TStatsConnection stats = SurviveChallenge.getInstance().getStatsConnection();
        List<UUID> players = stats.getTopPlayer(TStatsType.KILL, heads.size());
        TRankingObject[] sort_list = sortHeads();
        
        int rank_pos = 0;
        
        for(TRankingObject h : sort_list){
            if(h.getLocation().getBlock().getState() instanceof Skull){
                Skull sk = (Skull) h.getLocation().getBlock().getState();
                sk.setSkullType(SkullType.PLAYER);
                sk.setOwner(SurviveChallenge.getInstance().getUserAPI().getUser(players.get(rank_pos)).getName());
                sk.update(true);
            }
            rank_pos++;
            if(rank_pos >= players.size()){
                break;
            }
        }
        
    }*/
    public static RRankingObject[] sortByRank(List<RRankingObject> objects){
        RRankingObject[] array = new RRankingObject[objects.size()];
        array = objects.toArray(array);
        //Bukkit.broadcastMessage("" + array.length);
        
        RRankingObject temp = null;
        for(int run = 0; run <= array.length;run++){
            for(int pos = 0; pos < array.length-1;pos++){
                if(array[pos].getRankingPos() <= array[pos+1].getRankingPos()){
                    
                    Bukkit.broadcastMessage("Sort");
                    temp = array[pos];
                    array[pos] = array[pos+1];
                    array[pos+1] = temp;
                }
            }
        }
        return array;
    }
    public static RRankingObject getbyLocation(Location loc){
        for(RRankingObject object : objects){
            if(object.getLocation().equals(loc)){
                return object;
            }
        }
        return null;
    }
    
}
