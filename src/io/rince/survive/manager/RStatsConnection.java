/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.rince.survive.Survive;
import io.rince.survive.enums.RStatsType;
import io.rince.survive.userdatabase.UserProfile;
import io.rince.survive.util.RObjectRestore;

/**
 *
 * @author Rince
 */
public class RStatsConnection
{
    private final Survive plugin;
    
    public RStatsConnection(Survive instance){
        this.plugin = instance;
        if(plugin.getConnection().hasConnection()){
            plugin.getConnection().queryUpdate("CREATE TABLE IF NOT EXISTS s_stats(player VARCHAR(70),kills INT(5),deaths INT(5),points INT(10))");
            plugin.getConnection().queryUpdate("CREATE TABLE IF NOT EXISTS modul_stats(player VARCHAR(70),modul VARCHAR(20),games INT(10),wins INT(10))");
        }
    }
    
    public int getStats(UUID player,RStatsType type){
        int i = 0;
        if(plugin.getConnection().hasConnection()){
            try {
                Connection con = plugin.getConnection().getConnection();
                PreparedStatement st = null;
                ResultSet rs = null;
                
                st = con.prepareStatement("SELECT * FROM s_stats WHERE player='" + player.toString() + "'");
                rs = st.executeQuery();
                rs.last();
                if(rs.getRow() != 0){
                    rs.first();
                    i = rs.getInt(type.getTableRow());
                }
            } catch (SQLException ex) {
                Logger.getLogger(RStatsConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return i;
    }
    public boolean hasStatsSelection(UUID player){
        if(plugin.getConnection().hasConnection()){
            try {
                Connection con = plugin.getConnection().getConnection();
                PreparedStatement st = null;
                ResultSet rs = null;
                
                st = con.prepareStatement("SELECT * FROM s_stats WHERE player='" + player.toString() + "'");
                rs = st.executeQuery();
                rs.last();
                if(rs.getRow() != 0){
                    return true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(RStatsConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    public void createStatsSelection(UUID player){
        if(!hasStatsSelection(player)){
            plugin.getConnection().queryUpdate("INSERT INTO s_stats(player,kills,deaths,points) VALUES ('" + player.toString() + "',0,0,0)");
        }
    }
    public boolean hasPlayedModul(UUID player,String m_name){
        if(plugin.getConnection().hasConnection()){
            try {
                Connection con = plugin.getConnection().getConnection();
                PreparedStatement st = null;
                ResultSet rs = null;
                
                st = con.prepareStatement("SELECT * FROM modul_stats WHERE player='" + player.toString() + "' AND modul='" + m_name + "'");
                rs = st.executeQuery();
                rs.last();
                if(rs.getRow() != 0){
                    return true;
                }
            } catch (SQLException ex) {
                return false;
            }
        }
        return false;
    }
    
    public List<RObjectRestore> getPlayerModuls(UUID player){
        List<RObjectRestore> restore = new ArrayList<>();
        
        if(plugin.getConnection().hasConnection()){
            try {
                Connection con = plugin.getConnection().getConnection();
                PreparedStatement st = null;
                ResultSet rs = null;
                
                st = con.prepareStatement("SELECT * FROM modul_stats WHERE player='" + player.toString() + "'");
                rs = st.executeQuery();
                while(rs.next()){
                    RObjectRestore ob = new RObjectRestore();
                    
                    ob.addObject("name", rs.getString("modul"));
                    ob.addObject("games", rs.getInt("games"));
                    ob.addObject("wins", rs.getInt("wins"));
                    restore.add(ob);
                    
                    //Bukkit.broadcastMessage("Fetch Data [" + rs.getString("modul") + ";" + plugin.getUserAPI().getUser(player).getName() + "]");
                }
            } catch (SQLException ex) {
                Logger.getLogger(RStatsConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return restore;
    }
    
    public List<UserProfile> getTopPlayer(RStatsType type,int amount){
        List<UserProfile> list = new ArrayList<>();
        if(plugin.getConnection().hasConnection()){
            try {
                Connection con = plugin.getConnection().getConnection();
                PreparedStatement st = null;
                ResultSet rs = null;
                
                if(type == RStatsType.GAME || type == RStatsType.WIN){
                    st = con.prepareStatement("SELECT * FROM modul_stats ORDER BY " + type.getTableRow() + " DESC LIMIT " + amount);
                } else {
                    st = con.prepareStatement("SELECT * FROM s_stats ORDER BY " + type.getTableRow() + " DESC LIMIT " + amount);
                }
                
                rs = st.executeQuery();
                while(rs.next()){
                    list.add(plugin.getUserAPI().getUser(UUID.fromString(rs.getString("player"))));
                }
            } catch (SQLException ex) {
                Logger.getLogger(RStatsConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }
    
    /*private int[] performTopSort(TStatsType type){
        int[] basic = new int[TGamePlayer.getGamePlayers().length];
        
        int b = 0;
        for(TGamePlayer player : TGamePlayer.getGamePlayers()){
            switch(type){
                case DEATH:
                    basic[b] = player.getDeaths();
                    break;
                case POINT:
                    basic[b] = player.getPoints();
                    break;
                case KILL:
                    basic[b] = player.getKills();
                    break;
                case GAME:
                    basic[b] = player.calculateModulStats(TModulStatsType.GAME);
                    break;
                case WIN:
                    basic[b] = player.calculateModulStats(TModulStatsType.WIN);
                    break;
                default:
                    basic[b] = 0;
                    break;
                  
            }
            b++;
        }
        
        int temp = 0;
        for(int run = 0; run < basic.length;run++){
            for(int pos = 0; pos < basic.length-1;pos++){
                if(basic[pos] > basic[pos+1]){
                    temp = basic[pos];
                    basic[pos] = basic[pos+1];
                    basic[pos+1] = temp;
                }
            }
        }
        return basic;
    }*/
    
}
