/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.manager;

import io.rince.survive.Survive;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RGameTimer;



/**
 *
 * @author Rince
 */
public class RGameManager {
    
    
    @SuppressWarnings("unused")
	private final Survive plugin;
    private RGameStatus current;
    
    public RGameManager(Survive instance){
        this.plugin = instance;
        current = RGameStatus.BEFORE_LOBBY;
    }
    
    
    
    public void startGameTimer(RGameTimer timer,int between,boolean restart){
        if(!timer.isRunning()){
            timer.start(between, restart);
        }
    }
    public void stopGameTimer(RGameTimer timer,boolean reset_count){
        if(timer.isRunning()){
            timer.stop(reset_count);
        }
    }
    public void setCurrentGameStatus(RGameStatus status){
        this.current = status;
    }
    public RGameStatus getCurrentGameStatus(){
        return current;
    }
}
