/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.modul.RBasicModul;
import io.rince.survive.util.RGameMap;

/**
 *
 * @author Rince
 */
public class RVoteManager {
    
    
    private boolean voting;
    private final Survive plugin;
    private int count;
    private RBasicModul random,win;
    
    public RVoteManager(Survive instance){
        this.voting = false;
        this.plugin = instance;
        count = 0;
    }
    
    public void registerRandomMaps(){
        String s = plugin.convertPath(RConfigPath.SETTINGS_VOTE_MAP_AMOUNT).toString();
        int n;
        if(s.equalsIgnoreCase("auto")){
            n = RGameMap.getGameMaps().length;
        } else {
            n = Integer.valueOf(s);
        }
        
        if(n > RGameMap.getGameMaps().length){
            n = RGameMap.getGameMaps().length;
        }
        
        boolean same = n == RGameMap.getGameMaps().length;
        
        while(n != 0){
            RGameMap map = RGameMap.getRandomMap();
            if(same){
                map.setVoteMap(true);
                n--;
            } else {
                if(!map.isVoteMap()){
                    map.setVoteMap(true);
                    n--;
                }
            }
        }
    }
    @SuppressWarnings("deprecation")
	public void registerRandomModuls(){
        String s = plugin.convertPath(RConfigPath.SETTINGS_VOTE_MODUL_AMOUNT).toString();
        int n;
        if(s.equalsIgnoreCase("auto")){
            n = RBasicModul.getChallenges().length;
        } else {
            n = Integer.valueOf(s);
        }
        
        if(n > RBasicModul.getChallenges().length){
            n = RBasicModul.getChallenges().length;
        }
        while(n != 0){
            RBasicModul modul = RBasicModul.getRandomModul();
            if(!modul.isVoteModul()){
                modul.setVoteModul(true);
                n--;
            }
        }
    }
    
    public void showBoard(){
        Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective ob = sb.getObjective("vote");
        if(ob == null){
            ob = sb.registerNewObjective("vote", "dummy");
        }
        
        if(count <= (int) plugin.convertPath(RConfigPath.VOTE_BOARD_SWITCH_DELAY)/2){
            String name = plugin.convertPath(RConfigPath.MAP_VOTE_BOARD_NAME).toString().replace("&", "§");
            ob.setDisplayName(name);
            ob.setDisplaySlot(DisplaySlot.SIDEBAR);

            for(RGameMap map : RGameMap.getGameMaps()){
                if(!map.isVoteMap()){
                    continue;
                }
                String row = (String)plugin.convertPath(RConfigPath.MAP_VOTE_BOARD_ROW);
                row = row.replace("&", "§");
                row = row.replace("%id%", Integer.toString(map.getID()));
                row = row.replace("%map%", ChatColor.stripColor(map.getDisplayName().replace("&", "§")));

                ob.getScore(row).setScore(map.getVotes());
            }
        } else {
            if((boolean)plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
                String name = plugin.convertPath(RConfigPath.S_VOTE_BOARD_NAME).toString().replace("&", "§");
            
                ob.setDisplayName(name);
                ob.setDisplaySlot(DisplaySlot.SIDEBAR);

                for(RBasicModul challange : RBasicModul.getChallenges()){
                    if(!challange.isVoteModul()){
                        continue;
                    }
                    String row = (String)plugin.convertPath(RConfigPath.S_VOTE_BOARD_ROW);
                    row = row.replace("&", "§");
                    row = row.replace("%id%", Integer.toString(challange.getID()));
                    row = row.replace("%challenge%", ChatColor.stripColor(challange.getDisplayName().replace("&", "§")));

                    ob.getScore(row).setScore(challange.getVotes());
                }
            } else {
                this.count = 0;
            }
        }
        this.count++;
        if(count == (int) plugin.convertPath(RConfigPath.VOTE_BOARD_SWITCH_DELAY)*2){
            count = 0;
        }
        
        for(Player all : Bukkit.getOnlinePlayers()){
            all.setScoreboard(sb);
        }
    }
    public void setVote(boolean can_vote){
        this.voting = can_vote;
    }
    public boolean canVote(){
        return voting;
    }
    public RGameMap getWinnedMap(){
        RGameMap win = null;
        for(RGameMap map : RGameMap.getGameMaps()){
            if(win == null){
                win = map;
                continue;
            }
            if(win.getVotes() < map.getVotes()){
                win = map;
            }
        }
        return win;
    }
    public RBasicModul getWinnedChallenge(){
        if((boolean)plugin.convertPath(RConfigPath.SETTINGS_VOTE_MODUL_RANDOM)){
            if(this.random != null){
                return this.random;
            } else {
                this.random = RBasicModul.getChallenges()[(int)Math.random() * RBasicModul.getChallenges().length];
                return this.random;
            }
        }
        if(!(boolean) plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
            return null;
        }
        
        if(this.win != null){
            return this.win;
        }
        RBasicModul win = null;
        for(RBasicModul challenge : RBasicModul.getChallenges()){
            if(win == null){
                win = challenge;
                continue;
            }
            if(win.getVotes() < challenge.getVotes()){
                win = challenge;
            }
        }
        
        this.win = win;
        return win;
    }
    @SuppressWarnings("deprecation")
	public void activateWinnedChallenge(){
        RBasicModul c = getWinnedChallenge();
        if(c == null){
            return;
        }
        
        for(RBasicModul challenge : RBasicModul.getChallenges()){
            if(challenge.equals(c)){
                c.activateListener();
                continue;
            }
            Bukkit.getPluginManager().disablePlugin(challenge.getPlugin());
        }
    }
    public void clearBoard(){
        for(Player all : Bukkit.getOnlinePlayers()){
            all.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        }
    }
}
