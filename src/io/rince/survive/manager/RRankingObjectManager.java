/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.rince.survive.Survive;
import io.rince.survive.ranking.RRankingHead;
import io.rince.survive.ranking.RRankingObject;
import io.rince.survive.ranking.RRankingSign;

/**
 *
 * @author Rince
 */
public class RRankingObjectManager
{
    
    
    
    public static enum TRankingObjectType {
        
        RANKING_SIGN,
        RANKING_HEAD;
        
    }

    
    private File file = null;
    private final Survive plugin;
    
    public RRankingObjectManager(Survive instance){
        this.plugin = instance;
        this.file = new File(plugin.getDataFolder(),"heads.yml");
        if(!file.exists()){
            try
            {
                file.createNewFile();
            } catch (IOException ex)
            {
                Logger.getLogger(RRankingObjectManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void loadAll(){
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        List<String> heads;
        
        if((heads = cfg.getStringList("objects")) == null){
            return;
        }
        for(String s : heads){
            Location loc = toLocation(s);
            TRankingObjectType type = toObjectType(s);
            int rank = toRankingPos(s);
            
            if(type == TRankingObjectType.RANKING_HEAD){
                new RRankingHead(loc, rank);
            } else {
                new RRankingSign(loc, rank);
            }
        }
        
        /*TRankingSign.updateAll(Arrays.asList((TRankingObject[])TRankingSign.getRankingSigns()));
        TRankingHead.updateAll(Arrays.asList((TRankingObject[]) TRankingHead.getRankingHeads()));*/
        List<RRankingObject> h = new ArrayList<>();
        List<RRankingObject> h2 = new ArrayList<>();
        
        for(RRankingHead trh : RRankingHead.getRankingHeads()){
            h.add(trh);
        }
        for(RRankingSign trs : RRankingSign.getRankingSigns()){
            h2.add(trs);
        }
        
        RRankingObject.updateAll(h);
        RRankingObject.updateAll(h2);
    }
    
    // World,x,y,z
    public String toRankingString(Location loc,int ranking,TRankingObjectType type){
        return loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ() + ";" + ranking + ";" + type.toString();
    }
    public Location toLocation(String input){
        String[] values = input.split(";");
        String world = values[0];
        double x = Double.valueOf(values[1]);
        double y = Double.valueOf(values[2]);
        double z = Double.valueOf(values[3]);
        return new Location(Bukkit.getWorld(world), x, y, z);
    }
    public int toRankingPos(String input){
        return Integer.valueOf(input.split(";")[4]);
    }
    public TRankingObjectType toObjectType(String input){
        return TRankingObjectType.valueOf(input.split(";")[5]);
    }
    /*public boolean existsRankingObject(int rank,TRankingObjectType type){
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        List<String> list = cfg.getStringList("objects");
        if(list == null){
            return false;
        }
        for(String s : list){
            if(toRankingPos(s) == rank && toObjectType(s) == type){
                return true;
            }
        }
        return false;
    }*/
    public void removeRankingObjects(Location loc){
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        List<String> list = cfg.getStringList("objects");
        if(list == null){
            return;
        }
        
        List<String> ordinal = new ArrayList<>();
        for(String s : list){
            if(!toLocation(s).equals(loc)){
                ordinal.add(s);
            }
        }
        cfg.set("objects", ordinal);
        try
        {
            cfg.save(file);
        } catch (IOException ex)
        {
            Logger.getLogger(RRankingObjectManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void createRankingObject(Location loc,int ranking_pos,TRankingObjectType type){
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        List<String> list;
        if((list = cfg.getStringList("objects")) == null){
            list = new ArrayList<>();
        }
        removeRankingObjects(loc);
        list.add(toRankingString(loc, ranking_pos, type));
        if(type == TRankingObjectType.RANKING_HEAD){
            //TRankingHead head = TRankingHead.getbyRank(ranking_pos);
            new RRankingHead(loc, ranking_pos);
        } else if(type == TRankingObjectType.RANKING_SIGN){
            new RRankingSign(loc, ranking_pos);
        }
        
        cfg.set("objects", list);
        try
        {
            cfg.save(file);
        } catch (IOException ex)
        {
            Logger.getLogger(RRankingObjectManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
