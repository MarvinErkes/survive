/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

import io.rince.survive.Survive;
import io.rince.survive.manager.RRankingObjectManager;
import io.rince.survive.ranking.RRankingHead;
import io.rince.survive.ranking.RRankingObject;
import io.rince.survive.ranking.RRankingSign;

/**
 *
 * @author Rince
 */
public class RankingListener implements Listener
{
    
    public RankingListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void block_break(BlockBreakEvent e){
        RRankingObject ob;
        if((ob = RRankingObject.getbyLocation(e.getBlock().getLocation())) != null){
            Survive.getInstance().getRankingManager().removeRankingObjects(e.getBlock().getLocation());
            if(ob instanceof RRankingHead){
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + "Rankinghead wurde erfolgreich entfernt!");
                ((RRankingHead)ob).remove();
            }
            if(ob instanceof RRankingSign){
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + "Rankingschild wurde erfolgreich entfernt!");
                ((RRankingSign)ob).remove();
            }
        }
    }
    
    @EventHandler
    public void sign(SignChangeEvent e){
        if(e.getLine(0).equalsIgnoreCase("[Ranking]")){
            if(e.getLine(1).isEmpty()){
                e.setLine(1, "§c<pos>");
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + "Rangposition fehlt!");
                return;
            }
            try {
                int i = Integer.valueOf(e.getLine(1));
                Survive.getInstance().getRankingManager().createRankingObject(e.getBlock().getLocation(), i, RRankingObjectManager.TRankingObjectType.RANKING_SIGN);
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + "Rankingsign erstellt!");
            }catch(NumberFormatException ex){
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + "Ungültige Rangposition");
                e.setLine(1, "§c<pos>");
            }
        }
    }
    
}
