/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RGamePlayer;

/**
 *
 * @author Rince
 */
public class PlayerDeathListener implements Listener
{
    
    public PlayerDeathListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void death(final PlayerDeathEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.DEATH_MATCH || status == RGameStatus.IN_GAME){
            if(e.getEntity().getKiller() instanceof Player){
                String msg = Survive.getInstance().convertPath(RConfigPath.MESSAGE_PLAYER_DEATH).toString();
                msg = msg.replace("%entity%",e.getEntity().getDisplayName());
                if(e.getEntity().getKiller() instanceof Player){
                    msg = msg.replace("%killer%", e.getEntity().getKiller().getDisplayName());
                }
                msg = msg.replace("&", "§");

                e.setDeathMessage(Survive.getInstance().getPrefix() + msg);
                
                RGamePlayer killer = RGamePlayer.getGamePlayer(e.getEntity().getKiller().getUniqueId());
                killer.addKill();
            } else {
                String msg = Survive.getInstance().convertPath(RConfigPath.MESSAGE_PLAYER_DEATH_ALONE).toString();
                msg = msg.replace("%entity%",e.getEntity().getDisplayName());
                msg = msg.replace("&", "§");
                e.setDeathMessage(Survive.getInstance().getPrefix() + msg);
            }
            RGamePlayer player = RGamePlayer.getGamePlayer(e.getEntity().getUniqueId());
            player.setSpectator(true);
            player.addDeath();
            if((boolean) Survive.getInstance().convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
                player.insertModulStats(Survive.getInstance().getVoteManager().getWinnedChallenge().getName(), 1, 0);
            }
        }
    }
    @EventHandler
    public void respawn(PlayerRespawnEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.DEATH_MATCH || status == RGameStatus.IN_GAME){
            e.getPlayer().getInventory().clear();
            e.getPlayer().setAllowFlight(true);
            e.getPlayer().setFlying(true);
            e.getPlayer().getInventory().addItem(new ItemStack(Material.COMPASS));

            
            for(Player all : Bukkit.getOnlinePlayers()){
                RGamePlayer player = RGamePlayer.getGamePlayer(all.getUniqueId());
                if(player.isSpectator() || all.equals(e.getPlayer())){
                    continue;
                }
                all.hidePlayer(e.getPlayer());
            }
            
            
        }
    }
    
}
