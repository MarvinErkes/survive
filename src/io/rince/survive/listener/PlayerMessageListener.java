/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;
import io.rince.survive.util.RItemUtil;

/**
 *
 * @author Rince
 */
public class PlayerMessageListener implements Listener
{
    
    public PlayerMessageListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void join(final PlayerJoinEvent e){
        
        e.getPlayer().getInventory().clear();
        e.getPlayer().getInventory().setArmorContents(null);
        e.getPlayer().setHealth(20.0);
        e.getPlayer().setFoodLevel(20);
        
        Survive.getInstance().getUserAPI().createUser(e.getPlayer().getName(), e.getPlayer().getUniqueId());
        if(Survive.getInstance().getGameManager().getCurrentGameStatus() != RGameStatus.BEFORE_LOBBY && Survive.getInstance().getGameManager().getCurrentGameStatus() != RGameStatus.LOBBY){
            if(RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId()) == null){
                new RGamePlayer(e.getPlayer().getName(), e.getPlayer().getUniqueId(), true);
            }
            e.setJoinMessage(null);
            e.getPlayer().getInventory().addItem(new ItemStack(Material.COMPASS));
            e.getPlayer().teleport(Survive.getInstance().getVoteManager().getWinnedMap().getWorld().getSpawnLocation());
            e.getPlayer().setAllowFlight(true);
            e.getPlayer().setFlying(true);
            for(Player all : Bukkit.getOnlinePlayers()){
                if(all.equals(e.getPlayer()) || RGamePlayer.getGamePlayer(all.getUniqueId()).isSpectator()){
                    continue;
                }
                all.hidePlayer(e.getPlayer());
            }
        } else {
            String name = e.getPlayer().getName();

            if(RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId()) == null){
                new RGamePlayer(e.getPlayer().getName(), e.getPlayer().getUniqueId(), false);
            }
            
            e.getPlayer().setPlayerListName(name);
            e.getPlayer().setDisplayName(name);
            e.setJoinMessage(Survive.getInstance().getPrefix() + Survive.getInstance().convertPath(RConfigPath.MESSAGE_JOIN).toString().replace("&", "§").replace("%p%",name));
            if(Bukkit.getOnlinePlayers().size() >= (int) Survive.getInstance().convertPath(RConfigPath.SETTINGS_MIN_PLAYER)){
                RGameTimer.getGameTimer("lobby_count").start(1, true);
            }
        }
        
        if(e.getPlayer().isOp()){
                for(RGameMap map : RGameMap.getGameMaps()){
                    if(!map.isReady()){
                        e.getPlayer().sendMessage("§8[§cWarnung§8] §7Die Map '" + map.getName() + "' ist nicht vollständig!");
                    }
                }
            }
        
        int default_pos = 4;
        int diff = (int) Survive.getInstance().convertPath(RConfigPath.SETTINGS_VOTE_ITEM_POS_DIFF);
        boolean enable_m = (boolean) Survive.getInstance().convertPath(RConfigPath.SETTINGS_ENABLE_MODULS);
        
        if(!enable_m){
            diff = 0;
        }
        
        ItemStack i = RItemUtil.toItemStack(Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_ITEM).toString());
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_ITEM_NAME).toString().replace("&", "§"));
        i.setItemMeta(im);
        
        e.getPlayer().getInventory().setItem(default_pos-diff, i);
        
        if(enable_m){
            ItemStack i2 = RItemUtil.toItemStack(Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_ITEM).toString());
            ItemMeta im2 = i2.getItemMeta();
            im2.setDisplayName(Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_ITEM_NAME).toString().replace("&", "§"));
            i2.setItemMeta(im2);
            
            e.getPlayer().getInventory().setItem(default_pos+diff,i2);
        }
        
        if(Survive.getInstance().getLobby() == null){
            return;
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(Survive.getInstance(), new Runnable()
        {

            @Override
            public void run()
            {
                e.getPlayer().teleport(Survive.getInstance().getLobby());
            }
        },5);
    }
    
    @EventHandler
    public void quit(PlayerQuitEvent e){
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player.isSpectator()){
            e.setQuitMessage(null);
            RGamePlayer.removeGamePlayer(e.getPlayer().getUniqueId(), true);
        } else {
            String quit = Survive.getInstance().convertPath(RConfigPath.MESSAGE_QUIT).toString();
            quit = quit.replace("&", "§");
            quit = quit.replace("%p%", e.getPlayer().getDisplayName());
            e.setQuitMessage(Survive.getInstance().getPrefix() + quit);
        }
    }
    
    @EventHandler
    public void kick(PlayerKickEvent e) {
        RGamePlayer.removeGamePlayer(e.getPlayer().getUniqueId(), true);
    }
    
    @EventHandler
    public void chat(AsyncPlayerChatEvent e){
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player.isSpectator()){
            e.setCancelled(true);
            for(RGamePlayer all : RGamePlayer.getGamePlayers()){
                if(all.isSpectator()){
                    Bukkit.getPlayer(all.getUUID()).sendMessage("§8[§cX§8] §7" + e.getPlayer().getDisplayName() + ": §f" + e.getMessage());
                }
            }
        } else {
            e.setFormat("§7" + e.getPlayer().getDisplayName() + ": §f" + e.getMessage());
        }
    }
}
