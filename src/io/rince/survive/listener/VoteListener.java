/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.modul.RBasicModul;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RItemUtil;

/**
 *
 * @author Rince
 */
public class VoteListener implements Listener
{
    private Inventory map = null;
    private Inventory modul = null;
    
    @SuppressWarnings("static-access")
	public VoteListener(){
        Survive.getInstance().getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    
    @EventHandler
    public void click(InventoryClickEvent e){
        if(e.getInventory().getName().equalsIgnoreCase(Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_INV_NAME).toString().replace("&", "§"))){
            e.setCancelled(true);
            if(!Survive.getInstance().getVoteManager().canVote()){
            	String cannot_vote = Survive.getInstance().convertPath(RConfigPath.MESSAGE_CANNOT_VOTE).toString();
            	cannot_vote = cannot_vote.replace("&", "§");
                ((Player)e.getWhoClicked()).sendMessage(Survive.getInstance().getPrefix() + cannot_vote);
                e.getView().close();
                return;
            }
            
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR){
                ItemStack i = e.getCurrentItem();
                ItemMeta im = i.getItemMeta();
                RGameMap map = RGameMap.getByDisplayName(im.getDisplayName(),false);
                Player p = (Player) e.getWhoClicked();
                if(map != null){
                    if(RGameMap.hasAlreadyVoted(p.getName())){
                    	String already_vote = Survive.getInstance().convertPath(RConfigPath.MESSAGE_ALREADY_VOTED_MAP).toString();
                    	already_vote = already_vote.replace("&", "§");
                        p.sendMessage(Survive.getInstance().getPrefix() + already_vote);
                        return;
                    }
                	String voted_map = Survive.getInstance().convertPath(RConfigPath.MESSAGE_VOTED_MAP).toString();
                	voted_map = voted_map.replace("&", "§").replace("%map%", map.getDisplayName().replace("&", "§"));
                    p.sendMessage(Survive.getInstance().getPrefix() + voted_map);
                    map.addVote(p.getName());
                    p.getInventory().remove(p.getItemInHand());
                    e.getView().close();
                } else {
                	String not_found_map = Survive.getInstance().convertPath(RConfigPath.MESSAGE_MAP_NOT_FOUND).toString();
                	not_found_map = not_found_map.replace("&", "§");
                    p.sendMessage(Survive.getInstance().getPrefix() + not_found_map);
                }
            }
        } else if(e.getInventory().getName().equalsIgnoreCase(Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_INV_NAME).toString().replace("&", "§"))){
            e.setCancelled(true);
            if(!Survive.getInstance().getVoteManager().canVote()){
            	String cannot_vote = Survive.getInstance().convertPath(RConfigPath.MESSAGE_CANNOT_VOTE).toString();
            	cannot_vote = cannot_vote.replace("&", "§");
                ((Player)e.getWhoClicked()).sendMessage(Survive.getInstance().getPrefix() + cannot_vote);
                e.getView().close();
                return;
            }
            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR){
                ItemStack i = e.getCurrentItem();
                ItemMeta im = i.getItemMeta();
                RBasicModul modul = RBasicModul.getByDisplayName(im.getDisplayName(), false);
                Player p = (Player) e.getWhoClicked();
                if(modul != null){
                    if(RBasicModul.hasAlreadyVoted(p.getName())){
                    	String already_vote = Survive.getInstance().convertPath(RConfigPath.MESSAGE_ALREADY_VOTED_MODULE).toString();
                    	already_vote = already_vote.replace("&", "§");
                        p.sendMessage(Survive.getInstance().getPrefix() + already_vote);
                        return;
                    }
                    e.getView().close();
                    p.getInventory().remove(p.getItemInHand());
                    modul.addVote(p.getName());
                	String voted_module = Survive.getInstance().convertPath(RConfigPath.MESSAGE_VOTED_MODULE).toString();
                	voted_module = voted_module.replace("&", "§").replace("%module%", modul.getDisplayName().replace("&", "§"));
                    p.sendMessage(Survive.getInstance().getPrefix() + voted_module);
                } else {
                	String not_found_module = Survive.getInstance().convertPath(RConfigPath.MESSAGE_MODULE_NOT_FOUND).toString();
                	not_found_module = not_found_module.replace("&", "§");
                    p.sendMessage(Survive.getInstance().getPrefix() + not_found_module);
                }
            }
        }
    }
    @EventHandler
    public void interact(PlayerInteractEvent e){
        if(e.getAction().toString().contains("RIGHT")){
            Player p = e.getPlayer();
            if(p.getItemInHand() != null){
                ItemStack hand = p.getItemInHand();
                
                ItemStack m_vote = RItemUtil.toItemStack(Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_ITEM).toString());
                ItemMeta im_vote = m_vote.getItemMeta();
                im_vote.setDisplayName(Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_ITEM_NAME).toString().replace("&", "§"));
                m_vote.setItemMeta(im_vote);
                
                ItemStack mo_vote = RItemUtil.toItemStack(Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_ITEM).toString());
                ItemMeta im_mo = mo_vote.getItemMeta();
                im_mo.setDisplayName(Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_ITEM_NAME).toString().replace("&", "§"));
                mo_vote.setItemMeta(im_mo);
                
                if(hand.equals(m_vote)){
                    if(RGameMap.getGameMaps().length == 0){
                    	String no_maps = Survive.getInstance().convertPath(RConfigPath.MESSAGE_NO_MAPS).toString();
                    	no_maps = no_maps.replace("&", "§");
                        p.sendMessage(Survive.getInstance().getPrefix() + no_maps);
                        return;
                    }
                    if(this.map == null){
                        buildMapInventory();
                    }
                    p.openInventory(this.map);
                    e.setCancelled(true);
                }
                if(hand.equals(mo_vote)){
                    if(RBasicModul.getChallenges().length == 0){
                    	String no_modules = Survive.getInstance().convertPath(RConfigPath.MESSAGE_NO_MODULES).toString();
                        no_modules = no_modules.replace("&", "§");
                        p.sendMessage(Survive.getInstance().getPrefix() + no_modules);
                        return;
                    }
                    if(this.modul == null){
                        buildModulInventory();
                    }
                    p.openInventory(this.modul);
                    e.setCancelled(true);
                }
                
                
            }
        }
    }
    /*int lines = 0;
                    while(lines * 9 < TGameMap.getGameMaps().length){
                        lines++;
                    }
                    Inventory inv = Bukkit.createInventory(null, lines*9, "Voting - S");
                    int slot = 0;
                    for(TBasicModul modul : TBasicModul.getChallenges()){
                        ItemStack i = modul.getVoteItem();
                        ItemMeta im = i.getItemMeta();
                        im.setDisplayName(modul.getDisplayName().replace("&", "§"));
                        i.setItemMeta(im);
                        
                        inv.setItem(slot, i);
                        slot++;
                    }
                    p.openInventory(inv);
                    e.setCancelled(true);*/
    
    private void buildMapInventory(){
        int lines = 0;
        while(lines * 9 < RGameMap.getGameMaps().length){
            lines++;
        }
        
        this.map = Bukkit.createInventory(null, lines*9,Survive.getInstance().convertPath(RConfigPath.SETTINGS_MAP_VOTE_INV_NAME).toString().replace("&", "§"));
        int current = 0;
        for(RGameMap map : RGameMap.getGameMaps()){
            ItemStack i = map.getVoteItem();
            ItemMeta im = i.getItemMeta();
            im.setDisplayName(map.getDisplayName().replace("&", "§"));
            i.setItemMeta(im);
                        
            this.map.setItem(current, i);
            current++;
        }
        
    }
    private void buildModulInventory(){
        int lines = 0;
        while(lines * 9 < RBasicModul.getChallenges().length){
            lines++;
        }
        this.modul = Bukkit.createInventory(null, lines*9,Survive.getInstance().convertPath(RConfigPath.SETTINGS_S_VOTE_INV_NAME).toString().replace("&", "§"));
        
        int current = 0;
        for(RBasicModul modul : RBasicModul.getChallenges()){
            ItemStack i = modul.getVoteItem();
            ItemMeta im = i.getItemMeta();
            im.setLore(Arrays.asList("§7§o" + ChatColor.stripColor(modul.getDescription())));
            im.setDisplayName(modul.getDisplayName().replace("&", "§"));
            i.setItemMeta(im);
                        
            this.modul.setItem(current, i);
            current++;
        }
    }
}
