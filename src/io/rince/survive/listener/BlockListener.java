/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RBlockList;

/**
 *
 * @author Rince
 */
public class BlockListener implements Listener
{
    private static List<String> mode = new ArrayList<>();
    
    public BlockListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void break_block(BlockBreakEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.BEFORE_LOBBY || status == RGameStatus.LOBBY){
            e.setCancelled(true);
            return;
        }
        
        if(!e.getPlayer().hasPermission("survive.break")){
            if(Survive.getInstance().getBlockWhiteList() != null && Survive.getInstance().getBlockWhiteList().containsBlock(e.getBlock().getType())){
                e.setCancelled(false);
                return;
            }
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void place_block(BlockPlaceEvent e){
        if(!e.getPlayer().hasPermission("survive.place")){
            if(e.getBlock().getType() == Material.CAKE_BLOCK || e.getBlock().getType() == Material.MELON_BLOCK){
                e.setCancelled(false);
                return;
            }
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void add(PlayerInteractEvent e){
        if(!containsPlayer(e.getPlayer().getName())){
            return;
        }
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
            Block b = e.getClickedBlock();
            if(Survive.getInstance().getBlockWhiteList() == null){
                Survive.getInstance().setBlockWhiteList(new RBlockList());
            }
            if(Survive.getInstance().getBlockWhiteList().containsBlock(b.getType())){
                String block_removed = Survive.getInstance().convertPath(RConfigPath.MESSAGE_BLOCK_REMOVED).toString();
                block_removed = block_removed.replace("&", "§");
                e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + block_removed);
                Survive.getInstance().getBlockWhiteList().removeBlock(b.getType());
                return;
            }
            Survive.getInstance().getBlockWhiteList().addBlock(b.getType());
            String block_added = Survive.getInstance().convertPath(RConfigPath.MESSAGE_BLOCK_ADDED).toString();
            block_added = block_added.replace("&", "§");
            e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + block_added);
        }
    }
    public static void addPlayerToMode(String name){
        if(!mode.contains(name)){
            mode.add(name);
        }
    }
    public static void removePlayerFromMode(String name){
        if(mode.contains(name)){
            mode.remove(name);
        }
    }
    public static boolean containsPlayer(String name){
        return mode.contains(name);
    }
}
