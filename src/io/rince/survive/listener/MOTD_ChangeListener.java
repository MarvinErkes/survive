/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.server.ServerListPingEvent;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;

/**
 *
 * @author Rince
 */
public class MOTD_ChangeListener implements Listener
{
    
    public MOTD_ChangeListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void login(final PlayerLoginEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        /*if(status == TGameStatus.DEATH_MATCH || status == TGameStatus.DEATH_MATCH_GRACE_PERIOD || status == TGameStatus.DEATH_MATCH_ON_SPAWNS ||
                status == TGameStatus.GAME_GRACE_PERIOD || status == TGameStatus.GAME_ON_SPAWNS || status == TGameStatus.IN_GAME){
            
            
        } else {
            
        }*/
        if(status == RGameStatus.END){
            String game_is_ending = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_IS_ENDING).toString();
            game_is_ending = game_is_ending.replace("&", "§");
            e.disallow(null, Survive.getInstance().getPrefix() + game_is_ending);
        }
    }
    
    @EventHandler
    public void change_motd(ServerListPingEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.BEFORE_LOBBY || status == RGameStatus.LOBBY){
            String motd_lobby = Survive.getInstance().convertPath(RConfigPath.MOTD_LOBBY).toString();
            motd_lobby = motd_lobby.replace("&", "§");
            e.setMotd(motd_lobby);
        }
        if(status == RGameStatus.GAME_GRACE_PERIOD || status == RGameStatus.GAME_ON_SPAWNS || status == RGameStatus.IN_GAME){
            String motd_ingame = Survive.getInstance().convertPath(RConfigPath.MOTD_INGAME).toString();
            motd_ingame = motd_ingame.replace("&", "§");
            e.setMotd(motd_ingame);
        }
        if(status == RGameStatus.DEATH_MATCH || status == RGameStatus.DEATH_MATCH_GRACE_PERIOD || status == RGameStatus.DEATH_MATCH_ON_SPAWNS){
            String motd_deathmatch = Survive.getInstance().convertPath(RConfigPath.MOTD_DEATHMATCH).toString();
            motd_deathmatch = motd_deathmatch.replace("&", "§");
        	e.setMotd(motd_deathmatch);
        }
        if(status == RGameStatus.END){
            String motd_restarting = Survive.getInstance().convertPath(RConfigPath.MOTD_RESTARTING).toString();
            motd_restarting = motd_restarting.replace("&", "§");
        	e.setMotd(motd_restarting);
        }
    }
}
