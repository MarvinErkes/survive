/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;

/**
 *
 * @author Rince
 */
public class ItemAddListener implements Listener
{
    
    private static List<String> modus = new ArrayList<String>();
    
    public ItemAddListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void interact(PlayerInteractEvent e) throws IOException{
        if(e.getAction().toString().contains("RIGHT")){
            if(e.getPlayer().getItemInHand() != null){
                if(modus.contains(e.getPlayer().getName())){
                    File f = new File(Survive.getInstance().getDataFolder(),"items.yml");
                    FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
                    if(cfg.getConfigurationSection("items") == null){
                       cfg.createSection("items");
                    }
                    
                    for(String s : cfg.getConfigurationSection("items").getKeys(false)){
                        if(cfg.getItemStack("items." + s).equals(e.getPlayer().getItemInHand())){
                            cfg.set("items." + s, null);
                            cfg.save(f);
                            String item_removed = Survive.getInstance().convertPath(RConfigPath.MESSAGE_ITEM_REMOVED).toString();
                            item_removed = item_removed.replace("&", "§");
                            e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + item_removed);
                            return;
                        }
                    }
                    cfg.set("items." + (cfg.getConfigurationSection("items").getKeys(false).size()+1), e.getPlayer().getItemInHand());
                    cfg.save(f);
                    String item_added = Survive.getInstance().convertPath(RConfigPath.MESSAGE_ITEM_ADDED).toString();
                    item_added = item_added.replace("&", "§");
                    e.getPlayer().sendMessage(Survive.getInstance().getPrefix() + item_added);
                }
            }
        }
    }
    public static List<String> getPlayers(){
        return modus;
    }
}
