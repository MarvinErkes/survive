/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.util.RGamePlayer;

/**
 *
 * @author Rince
 */
public class SpectatorListener implements Listener
{
    
    public SpectatorListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void compass(PlayerInteractEvent e){
        if(!e.getAction().toString().contains("RIGHT")){
            return;
        }
        if(e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getType() != Material.COMPASS){
            return;
        }
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player == null){
            return;
        }
        if(player.isSpectator()){
            int lines = 0;
            while(lines * 9 < Bukkit.getOnlinePlayers().size()){
                lines++;
            }
            if(lines > 6){
                lines = 6;
            }
            String spectator_inventory_title = Survive.getInstance().convertPath(RConfigPath.SPECTATOR_INVENTORY_TITLE).toString();
            spectator_inventory_title = spectator_inventory_title.replace("&", "§");
            Inventory inv = Bukkit.createInventory(null, lines*9,spectator_inventory_title);
            int slot = 0;
            
            for(Player p : Bukkit.getOnlinePlayers()){
                if(p.equals(e.getPlayer())){
                    continue;
                }
                ItemStack skull = new ItemStack(Material.SKULL_ITEM,1,(short)3);
                ItemMeta im = skull.getItemMeta();
                im.setDisplayName("§6" + p.getName());
                skull.setItemMeta(im);
                
                inv.setItem(slot, skull);
                slot++;
            }
            e.getPlayer().openInventory(inv);
        }
    }
    @EventHandler
    public void inv(InventoryClickEvent e){
        String spectator_inventory_title = Survive.getInstance().convertPath(RConfigPath.SPECTATOR_INVENTORY_TITLE).toString();
        spectator_inventory_title = spectator_inventory_title.replace("&", "§");
        if(e.getInventory().getName().equalsIgnoreCase(spectator_inventory_title)){
            if(e.getCurrentItem()!= null && e.getCurrentItem().getType() != Material.AIR){
                Player p = (Player) e.getWhoClicked();
                String name = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                if(Bukkit.getPlayer(name) != null){
                    String spectator_now = Survive.getInstance().convertPath(RConfigPath.SPECTATOR_NOW).toString();
                    spectator_now = spectator_now.replace("&", "§").replace("%name%", name);
                    p.sendMessage(Survive.getInstance().getPrefix() + spectator_now);
                    p.teleport(Bukkit.getPlayer(name).getLocation());
                } else {
                    p.sendMessage(Survive.getInstance().getPrefix() + "Error");
                    e.getView().close();
                }
            }
        }
    }
    
    @EventHandler
    public void damage(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            Player damager = (Player) e.getDamager();
            RGamePlayer player = RGamePlayer.getGamePlayer(damager.getUniqueId());
            if(player != null && player.isSpectator()){
                e.setCancelled(true);
            }
        }
        if(e.getEntity() instanceof Player){
            Player entity = (Player) e.getEntity();
            RGamePlayer player = RGamePlayer.getGamePlayer(entity.getUniqueId());
            if(player != null && player.isSpectator()){
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void flight(PlayerToggleFlightEvent e){
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player == null){
            return;
        }
        if(player.isSpectator()){
            e.setCancelled(false);
            e.getPlayer().setAllowFlight(true);
            e.getPlayer().setFlying(true);
        }
    }
    
    @EventHandler
    public void picup(PlayerPickupItemEvent e){
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player != null && player.isSpectator()){
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void drop(PlayerDropItemEvent e){
        RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
        if(player != null && player.isSpectator()){
            e.setCancelled(true);
        }
    }
    
    
}
