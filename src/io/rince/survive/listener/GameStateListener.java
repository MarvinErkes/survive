/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.WorldSaveEvent;

import io.rince.survive.Survive;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RGamePlayer;

/**
 *
 * @author Rince
 */
public class GameStateListener implements Listener
{
    
    public GameStateListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @EventHandler
    public void damage(EntityDamageEvent e){
        if(e.getEntity() instanceof Player){
            RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
            if(status != RGameStatus.END && status != RGameStatus.DEATH_MATCH_GRACE_PERIOD && status != RGameStatus.BEFORE_LOBBY && status != RGameStatus.LOBBY && status != RGameStatus.GAME_GRACE_PERIOD && status != RGameStatus.DEATH_MATCH_ON_SPAWNS && status != RGameStatus.GAME_ON_SPAWNS){
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler
    public void hunger(FoodLevelChangeEvent e){
        if(!(e.getEntity() instanceof Player)){
            return;
        }
        Player p = (Player) e.getEntity();
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.BEFORE_LOBBY || status == RGameStatus.LOBBY || status == RGameStatus.DEATH_MATCH_GRACE_PERIOD || status == RGameStatus.DEATH_MATCH_ON_SPAWNS || status == RGameStatus.GAME_GRACE_PERIOD || status == RGameStatus.GAME_ON_SPAWNS){
            e.setCancelled(true);
        }
        RGamePlayer player = RGamePlayer.getGamePlayer(p.getUniqueId());
        if(player != null && player.isSpectator()){
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void drop(PlayerDropItemEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.BEFORE_LOBBY || status == RGameStatus.LOBBY){
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void move(PlayerMoveEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.DEATH_MATCH_ON_SPAWNS || status == RGameStatus.GAME_ON_SPAWNS){
            if(e.getFrom().getX() > e.getTo().getX() || e.getFrom().getZ() > e.getTo().getZ() || e.getFrom().getX() < e.getTo().getX() || e.getFrom().getZ() < e.getTo().getZ()){
                e.getPlayer().teleport(e.getFrom());
            }
        }
    }
    @EventHandler
    public void save(WorldSaveEvent e){
        Bukkit.broadcastMessage("Save World: " + e.getWorld().getName());
        System.out.println("Save World: " + e.getWorld().getName());
    }
   
}
