/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.enums;

/**
 *
 * @author Rince
 */
public enum RModulStatsType
{
    
    GAME("games"),
    WIN("wins");
    
    private String row;
    private RModulStatsType(String row){
        this.row = row;
    }
    
    public String getRestoreName(){
        return row;
    }
    
}
