/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.enums;

/**
 *
 * @author Rince
 */
public enum RGameStatus {
    
    BEFORE_LOBBY,
    GAME_ON_SPAWNS,
    GAME_GRACE_PERIOD,
    LOBBY,
    IN_GAME,
    DEATH_MATCH_ON_SPAWNS,
    DEATH_MATCH,
    DEATH_MATCH_GRACE_PERIOD,
    END;
    
}
