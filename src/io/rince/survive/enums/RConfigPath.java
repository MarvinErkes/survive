/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.enums;

import java.util.Arrays;

/**
 *
 * @author Rince
 */
public enum RConfigPath {
    
    PLUGIN_PREFIX("messages.prefix","&7[&eSurvive&7] "),
    
    MESSAGE_JOIN("messages.join","&e%p% &7hat das Spiel betreten!"),
    MESSAGE_QUIT("messages.quit","&e%p% &7hat das Spiel verlassen!"),
    MESSAGE_LOBBY_START("messages.lobby_start","Es sind nun genügend Spieler online!%n%Das Spiel startet in kürze..."),
    MESSAGE_LOBBY_BREAK("messages.lobby_break","Es sind nicht genügend Spieler online!%n%Der Lobbycountdown wurde abgebrochen!"),
    MESSAGE_LOBBY_COUNT("messages.lobby_count","In %count% Sekunden beginnt das Spiel!"),
    MESSAGE_VOTE_WIN("messages.vote_win","&7Das Voting wurde beendet!%n%Die Map &e%map% hat gewonnen!"),
    MESSAGE_MODUL_WIN("messages.modul_win","&7Es wird mit dem Modul &e%modul% &7gespielt!"),
    MESSAGE_GAME_COUNT("messages.game_start","Das Spiel startet in &e%count% &7Sekunden!"),
    MESSAGE_INFO_START("messages.info","Es wird auf der Map &e%map% &7gespielt.%n%Die Map wurde von &e%builder% &7gebaut"),
    MESSAGE_INFO_START2("messages.info2","Es wird mit dem Modul '%module%&7' gespielt.%n%Die Beschreibung des Moduls lautet: %description%"),
    MESSAGE_GAME_MOVE_COUNT("messages.game_move","In &e%count% &7Sekunden könnt ihr euch bewegen!"),
    MESSAGE_GAME_GREACE("messages.greace_periode","In &e%count% &7Sekunden könnt ihr kämpfen!"),
    MESSAGE_NOT_ENOTH_PLAYER("messages.not_enoth_player","Es sind nicht genügend Spieler online!"),
    MESSAGE_DEATHMATCH_COUNT("messages.deathmatch_count","Das &eDeathmatch &7startet in &e%count% &7Sekunden!"),
    MESSAGE_DEATHMATCH_GRACE_COUNT("messages.deathmatch_grace_count","In &e%count% &7Sekunden ist die Schutzzeit vorbei!"),
    MESSAGE_CHEST_REFILL("messages.chest_refill","Die &eKisten &7wurden wieder aufgefüllt!"),
    MESSAGE_SERVER_RESTART("messages.restart_server","Der Server restartet in &e%count% &7Sekunden!"),
    MESSAGE_NO_SQL_CONNECTION("messages.no_sql_connection","Es besteht &ekeine &7Verbindung zur MySQL-DB"),
    MESSAGE_GAME_IS_ENDING("messages.game_is_ending","&eDer Server restartet in wenigen Sekunden."),
    MESSAGE_PLAYER_DEATH("messages.player_death","&e%entity% &7wurde von &e%killer%&7 getötet!"),
    MESSAGE_PLAYER_DEATH_ALONE("messages.player_death_alone","&e%entity% &7ist gestorben!"),
    MESSAGE_BLOCK_ADDED("messages.block_added","Block wurde hinzugefügt!"),
    MESSAGE_BLOCK_REMOVED("messages.block_removed","Dieser Block war bereits vorhanden, er wurde entfernt!"),
    MESSAGE_ITEM_ADDED("messages.item_added","Item wurde hinzugefügt!"),
    MESSAGE_ITEM_REMOVED("messages.item_removed","Dieses Item war bereits vorhanden, es wurde entfernt!"),
    MESSAGE_NO_MODULES("messages.no_modules","Es wurden keine Module gefunden!"),
    MESSAGE_NO_MAPS("messages.no_maps","Es wurden keine Maps gefunden!"),
    MESSAGE_CANNOT_VOTE("messages.cannot_vote","Momentan kann nicht gevotet werden!"),
    MESSAGE_ALREADY_VOTED_MAP("messages.already_voted_map","Du hast bereits für eine Map gevotet!"),
    MESSAGE_ALREADY_VOTED_MODULE("messages.already_voted_module","Du hast bereits für ein Modul gevotet!"),
    MESSAGE_VOTED_MAP("messages.voted_map","Du hast erfolgreich für die Map '%map%&7' gevotet!"),
    MESSAGE_VOTED_MODULE("messages.voted_module","Du hast erfolgreich für das Modul '%module%&7' gevotet!"),
    MESSAGE_MAP_NOT_FOUND("messages.map_not_found","Diese Map ist nicht vorhanden!"),
    MESSAGE_MODULE_NOT_FOUND("messages.module_not_found","Dieses Modul ist nicht vorhanden!"),
    MESSAGE_GAME_ENDED("messages.game_ended","&eDas Spiel ist zuende!"),
    MESSAGE_GAME_WON("messages.game_won","&e%winner%&7 hat das Spiel gewonnen!"),
    MESSAGE_DM_END_NO_WIN("messages.dm_end_no_win","Das Deathmatch ist zuende, es konnte kein Gewinner festgestellt werden!"),
    MESSAGE_DM_ENDING("messages.dm_ending","&7Das Deathmatch endet in &e%count% &7Sekunden!"),
    MESSAGE_DM_STOP_NOT_ENOUGH_PLAYERS("messages.dm_stop_not_enough_players","Es sind nicht mehr genügend Spieler online,um das Deathmatch zu beginnen."),
    MESSAGE_GRACE_ENDING("messages.grace_ending","&7Die Friedensphase endet in &e%count% &7Sekunden!"),
    MESSAGE_GAME_STARTED("messages.game_started","Ihr könnt nun &ekämpfen!"),
    MESSAGE_GRACE_STOP_NOT_ENOUGH_PLAYERS("messages.grace_stop_not_enough_players","Es sind nicht mehr genügend Spieler online, das Spiel wird abgebrochen!"),
    MESSAGE_GRACE_ENDED("messages.grace_ended","Die Friedensphase ist zuende, jetzt kann gekämpft werden!"),
    MESSAGE_NOT_ENOUGH_SPAWNS("messages.not_enough_spawns","Es wurden nicht &egenügend &7Spawns gesetzt!"),
    MESSAGE_DM_FORCED("messages.dm_forced","Das Deathmatch startet in 60 Sekunden!"),
    MESSAGE_DM_STARTING("messages.dm_starting","&7Das Deathmatch startet in &e%count% &7Minute(n)!"),
    
    MOTD_LOBBY("motd.lobby","&aLobby"),
    MOTD_INGAME("motd.ingame","&cInGame"),
    MOTD_DEATHMATCH("motd.deathmatch","&cDeathMatch"),
    MOTD_RESTARTING("motd.restarting","&cRestarting"),
    
    MYSQL_USER("mysql.user","user"),
    MYSQL_PASSWORT("mysql.passwort","passwort"),
    MYSQL_DATABASE("mysql.database","database"),
    MYSQL_HOST("mysql.host","host"),
    
    
    SETTINGS_MIN_PLAYER("settings.min_player",4),
    SETTINGS_MAX_PLAYER("settings.max_player",12),
    SETTNGS_GAME_LENGHT("settings.game_lenght",25),
    SETTINGS_LOBBY_COUNT("settings.lobby_count",60),
    
    SETTINGS_DEATHMATCH_GRACE_COUNT("settings.deathmatch_grace",10),
    SETTINGS_DEATHMATCH_START_AMOUNT("settings.deathmatch_start",2),
    SETTINGS_GAME_GRACE("settings.game_grace",40),
    SETTINGS_GAME_MOVE_SEND("settings.send_game_move_at",5),
    SETTINGS_SERVER_FALLBACK("settings.fallback_server","Lobby"),
    SETTINGS_ENABLE_MODULS("settings.enable_moduls",true),
    
    
    SETTINGS_STATS_TOP_ORDER("statssystem.stats_order_type","kills"),
    SETTINGS_STATS_TOP_AMOUNT("statssystem.top_stats_amount",5),
    
    
    SETTINGS_VOTE_TO_COUNT("votesystem.to_count",25),
    SETTINGS_VOTE_MAP_AMOUNT("votesystem.map_amount",3),
    SETTINGS_VOTE_MODUL_RANDOM("votesystem.get_random_modul",false),
    SETTINGS_VOTE_MODUL_AMOUNT("votesystem.modul_amount","auto"),
    SETTINGS_VOTE_ITEM_POS_DIFF("votesystem.vote_item_pos_diff",2),
    
    SETTINGS_MAP_VOTE_ITEM("votesystem.map.item_id",339),
    SETTINGS_MAP_VOTE_ITEM_NAME("votesystem.map.item_name","&6Mapvoting"),
    SETTINGS_MAP_VOTE_INV_NAME("votesystem.map.inventory_name","&6Mapvoting"),
    
    SETTINGS_S_VOTE_ITEM("votesystem.modul.item_id",389),
    SETTINGS_S_VOTE_ITEM_NAME("votesystem.modul.item_name","&6Modulvoting"),
    SETTINGS_S_VOTE_INV_NAME("votesystem.modul.inventory_name","&6Modulvoting"),
    
    VOTE_BOARD_SWITCH_DELAY("voteboard.switch_delay",5),
    MAP_VOTE_BOARD_NAME("voteboard.map.name","&eVoting"),
    MAP_VOTE_BOARD_ROW("voteboard.map.row","&e(%id%) %map%"),
    
    
    S_VOTE_BOARD_NAME("voteboard.s.name","&eHerausforderungen"),
    S_VOTE_BOARD_ROW("voteboard.s.row","&e(%id%) %challenge%"),
    
    
    CHEST_ROWS("chest.rows",3),
    CHEST_NAME("chest.name","Chest"),
    CHEST_MAX_ITEMS("chest.max_items",6),
    CHEST_BLOCK("chest.block","54"),
    CHEST_REFILL("chest.enable_refill",true),
    CHEST_REFILL_TIME("chest.refill_time",18000),
    
    SPECTATOR_INVENTORY_TITLE("spectator.inventory.title","Spieler - Übersicht"),
    SPECTATOR_NOW("spectator.now", "Du beobachtest jetzt '&e%name%&7'"),
    
    RANKING_ORDER_TYPE("ranking.order_type","kills"),
    RANKING_SINGS_LINES("ranking.sings.lines",Arrays.asList("&l%name%","Kills: %kills%","Deaths: %deaths%"));
    
    private String path;
    private Object value;
    
    private RConfigPath(String path,Object default_value){
        this.path = path;
        this.value = default_value;
    }
    
    public String getPath(){
        return path;
    }
    public Object getDefaultValue(){
        return value;
    }
    
}
