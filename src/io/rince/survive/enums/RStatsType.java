package io.rince.survive.enums;

/*

 * Rince takes the waiting out of wanting. 

 */



/**
 *
 * @author Rince
 */
public enum RStatsType
{
    
    KILL("kills"),
    DEATH("deaths"),
    POINT("points"),
    GAME("games"),
    WIN("wins");
    
    private String row;
    private RStatsType(String row){
        this.row = row;
    }
    
    public String getTableRow(){
        return row;
    }
    public static RStatsType getBy(String name){
        
        for(RStatsType type : values()){
            String t = type.toString().toLowerCase();
            if((t + "s").equalsIgnoreCase(name) || t.equalsIgnoreCase(name)){
                return type;
            }
        }
        return KILL;
    }
    
}
