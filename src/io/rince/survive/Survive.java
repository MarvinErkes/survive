/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import io.rince.survive.chestmanager.RChestContainer;
import io.rince.survive.chestmanager.RChestListener;
import io.rince.survive.chestmanager.RChestManager;
import io.rince.survive.commands.MapCommands;
import io.rince.survive.commands.ModuleCommands;
import io.rince.survive.commands.StatsCommands;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.listener.BlockListener;
import io.rince.survive.listener.GameStateListener;
import io.rince.survive.listener.ItemAddListener;
import io.rince.survive.listener.PlayerDeathListener;
import io.rince.survive.listener.PlayerMessageListener;
import io.rince.survive.listener.RankingListener;
import io.rince.survive.listener.SpectatorListener;
import io.rince.survive.listener.VoteListener;
import io.rince.survive.manager.RBackupManager;
import io.rince.survive.manager.RDatabaseConnection;
import io.rince.survive.manager.RGameManager;
import io.rince.survive.manager.RRankingObjectManager;
import io.rince.survive.manager.RStatsConnection;
import io.rince.survive.manager.RVoteManager;
import io.rince.survive.timer.RBreakTimer;
import io.rince.survive.timer.RDMGraceTimer;
import io.rince.survive.timer.RDeathmatchTimer;
import io.rince.survive.timer.RGameGraceTimer;
import io.rince.survive.timer.RGameSpawnsTimer;
import io.rince.survive.timer.RInGameTimer;
import io.rince.survive.timer.RLobbyTimer;
import io.rince.survive.userdatabase.UserAPI;
import io.rince.survive.util.RBlockList;
import io.rince.survive.util.RDeathmatchArena;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RSpawnUtil;

public class Survive extends JavaPlugin {


	private static Survive instance;
	private RVoteManager avm;
	private RGameManager agm;
	private RDatabaseConnection mysql;
	private RStatsConnection stats_mysql;
	private UserAPI userapi;
	private RChestManager chest_manager;
	private RChestContainer global_container;
	private Location lobby;
	private RBackupManager backup;
	private RRankingObjectManager ranking_m;
	private RBlockList whitelist;

	public void onEnable() {
		loadConfig();
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

		instance = this;
		avm = new RVoteManager(instance);
		agm = new RGameManager(instance);
		chest_manager = new RChestManager(instance);
		global_container = new RChestContainer();
		backup = new RBackupManager(instance);
		ranking_m = new RRankingObjectManager(instance);

		new RChestListener();
		new BlockListener();
		new GameStateListener();
		new PlayerDeathListener();
		new SpectatorListener();
		new ItemAddListener();
		new VoteListener();
		new RankingListener();
		new PlayerMessageListener();

		new File(getDataFolder() + "/backups").mkdir();
		for(File f : new File(getDataFolder() + "/backups").listFiles()){
			try
			{
				if(!new File(f.getName()).exists()){
					getBackupManager().copyDir(f, new File(f.getName()));
				}
				Bukkit.createWorld(new WorldCreator(f.getName()));
			} catch (IOException ex)
			{
				Logger.getLogger(Survive.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		loadConfig();
		loadMaps();
		loadItems();
		loadBlockLists();

		mysql = new RDatabaseConnection(convertPath(RConfigPath.MYSQL_USER).toString(), convertPath(RConfigPath.MYSQL_PASSWORT).toString(),convertPath(RConfigPath.MYSQL_DATABASE).toString(), convertPath(RConfigPath.MYSQL_HOST).toString());
		mysql.openConnection();
		stats_mysql = new RStatsConnection(instance);
		userapi = new UserAPI(instance);
		userapi.createTable();

		getRankingManager().loadAll();


		new RBreakTimer(instance, "break_count", 10);
		new RDMGraceTimer(instance, "dm_grace",getConfig().getInt(RConfigPath.SETTINGS_DEATHMATCH_GRACE_COUNT.getPath()));
		new RDeathmatchTimer(instance,"dm_count", 60*4);
		new RGameGraceTimer(instance, "game_grace", getConfig().getInt(RConfigPath.SETTINGS_GAME_GRACE.getPath()));
		new RInGameTimer(instance, "game_count", 60*20);
		new RLobbyTimer(instance, "lobby_count", getConfig().getInt(RConfigPath.SETTINGS_LOBBY_COUNT.getPath()));
		new RGameSpawnsTimer(instance, "game_spawns_timer", 10);

		getChestManager().addContainer(global_container);

		getCommand("survive").setExecutor(new MapCommands(instance));
		getCommand("stats").setExecutor(new StatsCommands(instance));
		getCommand("module").setExecutor(new ModuleCommands(instance));

		for(Player all : Bukkit.getOnlinePlayers()){
			new RGamePlayer(all.getName(), all.getUniqueId(), false);
			for(Player all2 : Bukkit.getOnlinePlayers()){
				all.showPlayer(all2);
				all2.showPlayer(all);
			}

			all.setFlying(false);
			all.setAllowFlight(false);
		}
		new File(getDataFolder() + "/backups").mkdir();
	}


	@Override
	public void onDisable() {
		if(mysql.hasConnection()){
			mysql.closeConnection();
		}
		if(getBackupManager().isGameResetAllowed()){
			RGameMap gm = getVoteManager().getWinnedMap();
			getBackupManager().loadBackup(gm.getBackup());
			getBackupManager().loadBackup(gm.getDeathmatchArena().getBackup());
		}
		if(whitelist != null){
			File f = new File(getDataFolder(),"materials.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
			List<String> l = new ArrayList<>();
			for(Material m : whitelist.getBlocks()){
				l.add(m.toString());
			}
			cfg.set("whitelist", l);
			try
			{
				cfg.save(f);
			} catch (IOException ex)
			{
				Logger.getLogger(Survive.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}


	public static Survive getInstance(){
		return instance;
	}
	public RDatabaseConnection getConnection(){
		return mysql;
	}
	public UserAPI getUserAPI(){
		return userapi;
	}
	public Object convertPath(RConfigPath path){
		if(getConfig().isSet(path.getPath())){
			return getConfig().get(path.getPath());
		}
		return path.getDefaultValue();
	}
	public String getPrefix(){
		return (String) convertPath(RConfigPath.PLUGIN_PREFIX).toString().replace("&", "§");
	}

	public RVoteManager getVoteManager(){
		return avm;
	}
	public RGameManager getGameManager(){
		return agm;
	}
	public RBackupManager getBackupManager(){
		return backup;
	}
	public RStatsConnection getStatsConnection(){
		return stats_mysql;
	}
	public RChestManager getChestManager(){
		return chest_manager;
	}
	public RRankingObjectManager getRankingManager(){
		return ranking_m;
	}
	public RChestContainer getGlobalChestContainer(){
		return global_container;
	}
	public RBlockList getBlockWhiteList(){
		return whitelist;
	}
	public void setBlockWhiteList(RBlockList list){
		this.whitelist = list;
	}
	public Location getLobby(){
		return this.lobby;
	}
	public void setLobby(Location loc){
		this.lobby = loc;
	}


	private void loadConfig(){
		getConfig().options().copyDefaults(true);
		for(RConfigPath path : RConfigPath.values()){
			if(!getConfig().isSet(path.getPath())){
				getConfig().addDefault(path.getPath(), path.getDefaultValue());
			}
		}

		if(getConfig().isSet("lobby")){
			double x = getConfig().getDouble("lobby.x");
			double y = getConfig().getDouble("lobby.y");
			double z = getConfig().getDouble("lobby.z");
			float yaw = (float)getConfig().getDouble("lobby.yaw");
			float pitch = (float)getConfig().getDouble("lobby.pitch");
			this.lobby = new Location(Bukkit.getWorld(getConfig().getString("lobby.world")), x, y, z);
			this.lobby.setYaw(yaw);
			this.lobby.setPitch(pitch);
		}

		saveConfig();
	}
	@SuppressWarnings("deprecation")
	private void loadMaps(){

		File dir = new File(getDataFolder() + "/Arenen");
		dir.mkdir();

		for(File f : dir.listFiles()){
			if(f.getName().contains(".yml")){
				FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
				String name = f.getName().replace(".yml", "");

				String display = cfg.getString("settings.display");
				String builder = cfg.getString("settings.builder");
				String world = cfg.getString("data.world");

				RGameMap map = new RGameMap(Bukkit.getWorld(world), name, null);

				map.setDisplayName(display);
				map.setBuilder(builder);


				if(cfg.getStringList("data.spawns") != null){
					List<String> spawns = cfg.getStringList("data.spawns");
					for(String spawn : spawns){
						Location loc = RSpawnUtil.decompileSpawn(spawn);
						map.addSpawn(loc);
					}
				}
				
				RDeathmatchArena arena = null;
				if(cfg.getStringList("dm.spawns") != null){
					List<String> spawns = cfg.getStringList("dm.spawns");
					String[] values = spawns.get(0).split(";");

					String dmworld = values[0];
					
					Bukkit.createWorld(new WorldCreator(dmworld));
					for(String spawn : spawns){
						Location loc = RSpawnUtil.decompileSpawn(spawn);
						if(arena == null){

							arena = new RDeathmatchArena(instance, loc.getWorld());
						}
						arena.addSpawn(loc);
					}
				}
				map.setDeathmatchArena(arena);

				String item = cfg.getString("data.vitem");
				ItemStack set = null;
				if(item.contains(":")){
					String[] values = item.split(":");
					int id = Integer.valueOf(values[0]);
					Material m = Material.getMaterial(id);
					if(m == null){
						throw new NullPointerException("Material is not valid!");
					}
					short data = Short.valueOf(values[1]);
					set = new ItemStack(m,1,data);
				} else {
					int id = Integer.valueOf(item);
					Material m = Material.getMaterial(id);
					if(m == null){
						throw new NullPointerException("Material is not valid!");
					}
					set = new ItemStack(m);
				}
				map.setVoteItem(set);

			}
		}
	}
	public void loadItems(){
		File f = new File(getDataFolder(),"items.yml");
		if(!f.exists()){
			try
			{
				f.createNewFile();
			} catch (IOException ex)
			{
				Logger.getLogger(Survive.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
		if(cfg.isSet("items")){
			for(String s : cfg.getConfigurationSection("items").getKeys(false)){
				ItemStack i = cfg.getItemStack("items." + s);
				getGlobalChestContainer().addItemStack(i);
			}
		}
	}
	public void loadBlockLists() throws NullPointerException{
		File f = new File(getDataFolder(),"materials.yml");
		if(!f.exists()){
			try
			{
				f.createNewFile();
			} catch (IOException ex)
			{
				Logger.getLogger(Survive.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
		if(cfg.getStringList("whitelist") != null){
			this.whitelist = new RBlockList();
			for(String s : cfg.getStringList("whitelist")){
				Material m;
				if((m = Material.valueOf(s)) != null){
					this.whitelist.addBlock(m);
				}
			}
		}
	}
}
