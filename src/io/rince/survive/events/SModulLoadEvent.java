/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Rince
 */
public class SModulLoadEvent extends Event implements Cancellable
{
    
    public String modul_name;
    private boolean c;
    private static final HandlerList handlers = new HandlerList();

    public SModulLoadEvent(String modul_name){
        this.modul_name = modul_name;
        this.c = false;
    }
    
    @Override
    public HandlerList getHandlers()
    {
        return handlers;
    }

    @Override
    public boolean isCancelled()
    {
        return c;
    }

    @Override
    public void setCancelled(boolean c)
    {
        this.c = c;
    }
    public static final HandlerList getHandlerList(){
        return handlers;
    }
    
    public String getModulName(){
        return modul_name;
    }
    
    
    
}
