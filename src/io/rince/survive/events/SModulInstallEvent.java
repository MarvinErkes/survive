/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.events;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Rince
 */
public class SModulInstallEvent extends Event implements Cancellable
{
    
    private boolean c;
    private static final HandlerList handlers = new HandlerList();
    private String modul_name;
    private URL url;
    
    public SModulInstallEvent(String modul_name){
        try
        {
            this.modul_name = modul_name;
            this.url = new URL("http://37.10.112.20/survive/modules/" + modul_name + ".jar");
            this.c = false;
        } catch (MalformedURLException ex)
        {
            Logger.getLogger(SModulInstallEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public HandlerList getHandlers()
    {
        return handlers;
    }

    @Override
    public boolean isCancelled()
    {
        return c;
    }

    @Override
    public void setCancelled(boolean c)
    {
        this.c = c;
    }
    
    public static final HandlerList getHandlerList(){
        return handlers;
    }
    
    public String getModulName(){
        return modul_name;
    }
    public URL getDownloadURL(){
        return url;
    }
    
    
    
}
