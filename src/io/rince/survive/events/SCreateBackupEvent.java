/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.events;

import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Rince
 */
public class SCreateBackupEvent extends Event
{
    
    private World backup;
    private static final HandlerList handlers = new HandlerList();
    
    public SCreateBackupEvent(World backup){
        this.backup = backup;
    }

    @Override
    public HandlerList getHandlers()
    {
        return handlers;
    }
    public static final HandlerList getHandlerList(){
        return handlers;
    }
    public World getBackupWorld(){
        return backup;
    }
    
}
