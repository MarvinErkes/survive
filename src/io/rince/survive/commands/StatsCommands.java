/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RStatsType;
import io.rince.survive.modul.RBasicModul;
import io.rince.survive.userdatabase.UserProfile;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RObjectRestore;

/**
 *
 * @author Rince
 */
public class StatsCommands implements CommandExecutor
{

    private final Survive plugin;
    
    public StatsCommands(Survive instance){
        this.plugin = instance;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        
        if(!(sender instanceof Player)){
            return true;
        }
        Player p = (Player) sender;
        
        if(!plugin.getConnection().hasConnection()){
            plugin.getVoteManager().getWinnedMap().getWorld().setTime(0);
            String no_sql_connection = Survive.getInstance().convertPath(RConfigPath.MESSAGE_CHEST_REFILL).toString();
            no_sql_connection = no_sql_connection.replace("&", "§");
            p.sendMessage(plugin.getPrefix() + no_sql_connection);
            return true;
        }
        
        if(cmd.getName().equalsIgnoreCase("stats")){
            if(args.length == 0){
                if(p.hasPermission("survive.stats")){
                    RGamePlayer player = RGamePlayer.getGamePlayer(p.getUniqueId());
                    p.sendMessage(plugin.getPrefix() + "Kills: §e" + player.getKills());
                    p.sendMessage(plugin.getPrefix() + "Deaths: §e" + player.getDeaths());
                    if(RBasicModul.getChallenges().length > 0){
                        p.sendMessage(plugin.getPrefix() + "Modules: ");
                        for(RObjectRestore ob : player.getModulStats()){
                            p.sendMessage("§8- §e" + ob.getObject("name"));
                            p.sendMessage(" §8- §7Games: §e" + ob.getObject("games"));
                            p.sendMessage(" §8- §7Wins:  §e" + ob.getObject("wins"));
                        }
                    }
                    return true;
                } 
            } else if(args[0].equalsIgnoreCase("top")){
                if(p.hasPermission("survive.stats.top")){
                    String order = plugin.convertPath(RConfigPath.SETTINGS_STATS_TOP_ORDER).toString();
                    RStatsType type = RStatsType.getBy(order);
                    
                    List<UserProfile> players = plugin.getStatsConnection().getTopPlayer(type, (int) plugin.convertPath(RConfigPath.SETTINGS_STATS_TOP_AMOUNT));
                    String space = "                    ";
                    for(UserProfile profile : players){
                        int auto = plugin.getStatsConnection().getStats(profile.getUUID(), type);
                        
                        p.sendMessage(plugin.getPrefix() + profile.getName() + space.substring(profile.getName().length(),space.length()) + " | " + type.getTableRow() + ": §e" + auto);
                    }
                    return true;
                }
            } else {
                p.sendMessage("Coming Soon!");
                return true;
            }
        }
        
        return true;
    }
    
}
