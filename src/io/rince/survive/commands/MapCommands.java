/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.listener.BlockListener;
import io.rince.survive.listener.ItemAddListener;
import io.rince.survive.manager.RRankingObjectManager;
import io.rince.survive.util.RDeathmatchArena;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RSpawnUtil;

/**
 *
 * @author Rince
 */
public class MapCommands implements CommandExecutor
{
    private final Survive plugin;
    
    public MapCommands(Survive instance){
        this.plugin = instance;
    }

    @SuppressWarnings({ "deprecation", "unused" })
	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        
        if(!(sender instanceof Player)){
            return true;
        }
        Player p = (Player) sender;
        
        if(!plugin.getConnection().hasConnection()){
            String no_sql_connection = Survive.getInstance().convertPath(RConfigPath.MESSAGE_CHEST_REFILL).toString();
            no_sql_connection = no_sql_connection.replace("&", "§");
            p.sendMessage(plugin.getPrefix() + no_sql_connection);
            return true;
        }
        
        if(cmd.getName().equalsIgnoreCase("survive")){
            if(args.length == 0){
                if(p.hasPermission("survive.info")){
                    p.sendMessage(plugin.getPrefix() + "Name: §eSurvive");
                    p.sendMessage(plugin.getPrefix() + "Author: §eRince Devteam");
                    p.sendMessage(plugin.getPrefix() + "Version: §e" + plugin.getDescription().getVersion());
                    p.sendMessage(plugin.getPrefix() + "Build: §eALPHA-BUILD");
                    p.sendMessage(plugin.getPrefix() + "Help: §e/survive help");
                    return true;
                }
                
            } else if(args[0].equalsIgnoreCase("addarena")){
                // s addarena [name] [builder] 
                if(p.hasPermission("survive.addarena")){
                    if(args.length == 6){
                        try {
                            String name = args[1];
                            String display = args[2];
                            String builder = args[3];
                            String item = args[4];
                            String world = args[5];
                            
                            
                            File f = new File(plugin.getDataFolder() + "/Arenen",name + ".yml");
                            if(f.exists()){
                                p.sendMessage(plugin.getPrefix() + "§cDiese Arena existiert bereits!");
                                return true;
                            }
                            if(RGameMap.getByDisplayName(display, false) != null){
                                p.sendMessage(plugin.getPrefix() + "Dieser Displayname ist bereits vergeben!");
                                return true;
                            }
                            if(Bukkit.getWorld(world)==null){
                            	Bukkit.createWorld(new WorldCreator(world));
                                p.sendMessage(plugin.getPrefix() + "Diese Welt ist nicht vorhanden!");
                                p.sendMessage(plugin.getPrefix() + "Wenn die Welt im Ordner war wird sie nur importiert, gebe es erneut ein.");
                                p.teleport(Bukkit.getWorld(world).getSpawnLocation());
                                return true;
                            }
                            
                            
                            f.createNewFile();
                            FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
                            try{
                                int id = 0;
                                
                                if(item.contains(":")){
                                    String[] values = item.split(":");
                                    id = Integer.valueOf(values[0]);
                                } else {
                                    id = Integer.valueOf(item);
                                }
                                
                                if(Material.getMaterial(id) == null){
                                    p.sendMessage(plugin.getPrefix() + "Dieses Material ist ungültig!");
                                    return true;
                                }
                                cfg.set("data.vitem", item);
                            }catch(NumberFormatException e){
                                p.sendMessage(plugin.getPrefix() + "Die Item-ID ist fehlerhaft!");
                                return true;
                            }
                            cfg.set("settings.display", display);
                            cfg.set("settings.builder", builder);
                            cfg.set("data.world", world);
                            cfg.save(f);
                            p.sendMessage(plugin.getPrefix() + "Arena erstellt!");
                            
                            
                            RGameMap map = new RGameMap(p.getWorld(), name, null);
                            map.setBuilder(builder);
                            map.setDisplayName(display);
                            return true;
                        } catch (IOException ex) {
                            Logger.getLogger(MapCommands.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive addarena <name> <displayname> <builder> <vote-item-id> <world>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("addspawn")){
                if(p.hasPermission("survive.addspawn")){
                    if(args.length == 2){
                        try {
                            String arena = args[1];
                            if(RGameMap.getGameMap(arena) == null){
                                p.sendMessage(plugin.getPrefix() + "Diese Arena ist nicht vorhanden!");
                                return true;
                            }
                            File f = new File(plugin.getDataFolder() + "/Arenen",arena + ".yml");
                            if(!f.exists()){
                                return true;
                            }
                            FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
                            List<String> spawns = cfg.getStringList("data.spawns");
                            if(spawns == null){
                                spawns = new ArrayList<>();
                            }
                            spawns.add(RSpawnUtil.compileSpawn(p.getLocation()));
                            RGameMap.getGameMap(arena).addSpawn(p.getLocation());
                            cfg.set("data.spawns", spawns);
                            cfg.save(f);
                            p.sendMessage(plugin.getPrefix() + "Spawn wurde gesetzt §e(" + spawns.size() + ")");
                            return true;
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive addspawn <arena>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("maps")){
                if(p.hasPermission("survive.maps")){
                    if(RGameMap.getGameMaps().length == 0){
                        p.sendMessage(plugin.getPrefix() + "Es wurde §ekeine §7Arenen gefunden!");
                        return true;
                    }
                    for(RGameMap map : RGameMap.getGameMaps()){
                        p.sendMessage(plugin.getPrefix() + "- §e" + map.getName());
                    }
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("delarena")){
                if(p.hasPermission("survive.delarena")){
                    if(args.length == 2){
                        RGameMap map = RGameMap.getGameMap(args[1]);
                        if(map == null){
                            p.sendMessage(plugin.getPrefix() + "Diese Arena ist §enicht §7vorhanden!");
                            return true;
                        }
                        
                        new File(plugin.getDataFolder() + "/Arenen",args[1] + ".yml").delete();
                        RGameMap.deleteMap(args[1]);
                        p.sendMessage(plugin.getPrefix() + "Die Arena \"§e" + args[1] + "§7\" wurde gelöscht!");
                        return true;
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive delarena <name>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("imode")){
                if(p.hasPermission("survive.imode")){
                    if(ItemAddListener.getPlayers().contains(p.getName())){
                        p.sendMessage(plugin.getPrefix() + "Du bist nun nicht mehr im §eItemModus.");
                        ItemAddListener.getPlayers().remove(p.getName());
                        return true;
                    } else {
                        p.sendMessage(plugin.getPrefix() + "Du bist nun im §eItemModus.");
                        p.sendMessage(plugin.getPrefix() + "Mach Rechtsklick mit einem §eItem§7, um es hinzuzufügen!");
                        ItemAddListener.getPlayers().add(p.getName());
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("bmode")){
                if(p.hasPermission("survive.bmode")){
                    if(ItemAddListener.getPlayers().contains(p.getName())){
                        p.sendMessage(plugin.getPrefix() + "Bitte verlasse zuerst den ItemMode!");
                        return true;
                    }
                    if(BlockListener.containsPlayer(p.getName())){
                        p.sendMessage(plugin.getPrefix() + "Du bist nun nicht mehr im §eBlockModus.");
                        BlockListener.removePlayerFromMode(p.getName());
                        return true;
                    } else {
                        p.sendMessage(plugin.getPrefix() + "Du bist nun im §eBlockModus.");
                        p.sendMessage(plugin.getPrefix() + "Mach Rechtsklick auf einen §eBlock§7, um ihn hinzuzufügen!");
                        BlockListener.addPlayerToMode(p.getName());
                        return true;
                    }
                }
            }else if(args[0].equalsIgnoreCase("adddmspawn")){
                if(p.hasPermission("survive.adddmspawn")){
                    if(args.length == 2){
                        try {
                            RGameMap map = RGameMap.getGameMap(args[1]);
                            if(map == null){
                                p.sendMessage(plugin.getPrefix() + "Diese Arena ist §enicht §7vorhanden!");
                                return true;
                            }
                            File f = new File(plugin.getDataFolder() + "/Arenen",args[1] + ".yml");
                            FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
                            List<String> spawns = cfg.getStringList("dm.spawns");
                            if(spawns == null){
                                spawns = new ArrayList<>();
                            }
                            if(map.getDeathmatchArena() == null){
                                map.setDeathmatchArena(new RDeathmatchArena(plugin, p.getWorld()));
                            }
                            spawns.add(RSpawnUtil.compileSpawn(p.getLocation()));
                            map.getDeathmatchArena().addSpawn(p.getLocation());
                            cfg.set("dm.spawns", spawns);
                            cfg.save(f);
                            p.sendMessage(plugin.getPrefix() + "Spawn wurde hinzugefügt §e(" + spawns.size() + ")");
                            return true;
                        } catch (IOException ex) {
                            Logger.getLogger(MapCommands.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive adddmspawn <arena>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("setlobby")){
                if(p.hasPermission("survive.setlobby")){
                    FileConfiguration cfg = plugin.getConfig();
                    cfg.set("lobby.x", p.getLocation().getX());
                    cfg.set("lobby.y", p.getLocation().getY());
                    cfg.set("lobby.z", p.getLocation().getZ());
                    cfg.set("lobby.yaw", p.getLocation().getYaw());
                    cfg.set("lobby.pitch", p.getLocation().getPitch());
                    cfg.set("lobby.world", p.getWorld().getName());
                    plugin.saveConfig();
                    p.sendMessage(plugin.getPrefix() + "Lobby wurde gesetzt!");
                    plugin.setLobby(p.getLocation());
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("cbackup")){
                if(p.hasPermission("survive.backup.create")){
                    if(args.length < 2){
                        p.sendMessage(plugin.getPrefix() + "§e/survive cbackup <arena>");
                        return true;
                    }
                    RGameMap map = RGameMap.getGameMap(args[1]);
                    if(map == null){
                        p.sendMessage(plugin.getPrefix() + "Diese Arena existiert nicht!");
                        return true;
                    }
                    p.sendMessage(plugin.getPrefix() + "Backup wird erstellt...");
                    plugin.getBackupManager().createBackup(map.getWorld().getName());
                    if(map.getDeathmatchArena() != null){
                        plugin.getBackupManager().createBackup(map.getDeathmatchArena().getWorld().getName());
                    }
                    p.sendMessage(plugin.getPrefix() + "Backup wurde erstellt!");
                    return true;
                }
                
            } else if(args[0].equalsIgnoreCase("lbackup")){
                if(p.hasPermission("survive.backup.load")){
                    if(args.length < 2){
                        p.sendMessage(plugin.getPrefix() + "§e/survive lbackup <arena>");
                        return true;
                    }
                    RGameMap map = RGameMap.getGameMap(args[1]);
                    if(map == null){
                        p.sendMessage(plugin.getPrefix() + "Diese Arena existiert nicht!");
                        return true;
                    }
                    if(!plugin.getBackupManager().existsBackup(p.getWorld().getName())){
                        p.sendMessage(plugin.getPrefix() + "Backup wurde nicht gefunden! [World]");
                        return true;
                    }
                    if(map.getBackup() == null){
                        p.sendMessage(plugin.getPrefix() + "Backup wurde nicht gefunden! [Map]");
                        return true;
                    }
                    
                    
                    String world = p.getWorld().getName();
                    for(Player all : map.getWorld().getPlayers()){
                        all.teleport(Bukkit.getWorld("world").getSpawnLocation());
                    }
                    p.sendMessage(plugin.getPrefix() + "Backup wird geladen...");
                    plugin.getBackupManager().loadBackup(map.getBackup());
                    p.sendMessage(plugin.getPrefix() + "Backup geladen!");
                    return true;
                }
            } else if(args[0].equalsIgnoreCase("check")){
                if(p.hasPermission("survive.check")){
                    if(args.length == 2){
                        String arena = args[1];
                        RGameMap map = RGameMap.getGameMap(arena);
                        if(map == null){
                            p.sendMessage(plugin.getPrefix() + "Diese Arena ist §enicht §7vorhanden!");
                            return true;
                        }
                        map.sendReadyMessage(p);
                        
                        
                        return true;
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive check <arena>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("sethead")){
                if(p.hasPermission("survive.sethead")){
                    if(args.length == 2){
                        try{
                            int rank = Integer.valueOf(args[1]);
                            plugin.getRankingManager().createRankingObject(p.getLocation(), rank, RRankingObjectManager.TRankingObjectType.RANKING_HEAD);
                            p.sendMessage(plugin.getPrefix() + "Rankinghead erstellt!");
                            return true;
                        } catch(NumberFormatException e){
                            p.sendMessage(plugin.getPrefix() + "Ungültiger Rang");
                            return true;
                        }
                    } else {
                        p.sendMessage(plugin.getPrefix() + "§e/survive sethead <ranking-pos>");
                        return true;
                    }
                }
            } else if(args[0].equalsIgnoreCase("help")){
                if(p.hasPermission("survive.info")){
                    p.sendMessage(plugin.getPrefix() + "§e/survive");
                    p.sendMessage(plugin.getPrefix() + "§e/survive help");
                    p.sendMessage(plugin.getPrefix() + "§e/survive addarena <name> <displayname> <builder> <vote-item-id> <world>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive delarena <arena>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive addspawn <arena>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive adddmspawn <arena>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive sethead <pos>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive check <arena>");
                    p.sendMessage(plugin.getPrefix() + "§e/survive setlobby");
                    p.sendMessage(plugin.getPrefix() + "§e/survive cbackup");
                    p.sendMessage(plugin.getPrefix() + "§e/modul");
                    p.sendMessage(plugin.getPrefix() + "§e/stats");
                    return true;
                }
            }
        }
        
        
        return false;
    }
    
}
