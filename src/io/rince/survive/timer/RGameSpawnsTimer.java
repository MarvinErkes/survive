/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.modul.RBasicModul;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RGameSpawnsTimer extends RGameTimer
{
    private final Survive plugin;

    public RGameSpawnsTimer(Survive instance ,String name, int count)
    {
        super(name, count);
        this.plugin = instance;
    }

    @SuppressWarnings("deprecation")
	@Override
    public boolean performHeader()
    {
        plugin.getGameManager().setCurrentGameStatus(RGameStatus.GAME_ON_SPAWNS);
        RGameMap map = plugin.getVoteManager().getWinnedMap();
        if(Bukkit.getOnlinePlayers().size() > map.getSpawns().length){
			String not_enough_spawns = Survive.getInstance().convertPath(RConfigPath.MESSAGE_NOT_ENOUGH_SPAWNS).toString();
			not_enough_spawns = not_enough_spawns.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + not_enough_spawns);
            getGameTimer("break_count").start(1, false);
            return false;
        }
        
        RBasicModul c = null;
        if((boolean)plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
            if((c = plugin.getVoteManager().getWinnedChallenge()) != null){
                plugin.getVoteManager().getWinnedChallenge().activateListener();
            }
        }
        
        for(Player p : Bukkit.getOnlinePlayers()){
            if(RGamePlayer.getGamePlayer(p.getUniqueId()).isSpectator()){
                continue;
            }
            p.setFoodLevel(20);
            p.setHealth(20.0);
            p.getInventory().clear();
        }
        map.teleportAll();
        map.getWorld().setTime(0);
        
        if((boolean)plugin.convertPath(RConfigPath.CHEST_REFILL)){
            plugin.getChestManager().startRefill((int)plugin.convertPath(RConfigPath.CHEST_REFILL_TIME));
        }
        
        RGameMap win = plugin.getVoteManager().getWinnedMap();
        String info = plugin.convertPath(RConfigPath.MESSAGE_INFO_START).toString();
        info = info.replace("&", "§");
        info = info.replace("%map%", win.getDisplayName().replace("&", "§"));
        info = info.replace("%builder%", win.getBuilder());
        info = info.replace("%n%", "\n" + plugin.getPrefix());
        
        String info2 = plugin.convertPath(RConfigPath.MESSAGE_INFO_START2).toString();
        info2 = info2.replace("&", "§");
        info2 = info2.replace("%module%", c.getDisplayName().replace("&", "§"));
        info2 = info2.replace("%description%", c.getDescription());
        info2 = info2.replace("%n%", "\n" + plugin.getPrefix());
        
        if((boolean) plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS) && c != null){
            for(int i = 0; i < 100;i++){
                for(Player p : Bukkit.getOnlinePlayers()){
                    p.sendMessage(" ");
                }
            }
            Bukkit.broadcastMessage(plugin.getPrefix() + info);
            Bukkit.broadcastMessage(plugin.getPrefix() + info2);
        }
        
        return true;
    }

    @Override
    public void performBody()
    {
        if(Bukkit.getOnlinePlayers().size() < (int) plugin.convertPath(RConfigPath.SETTINGS_MIN_PLAYER)){
            Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_NOT_ENOTH_PLAYER).toString().replace("&", "§"));
            stop(false);
            getGameTimer("break_count").start(1, false);
            return;
        }
        if(getCount() < (int) plugin.convertPath(RConfigPath.SETTINGS_GAME_MOVE_SEND) && getCount() > 0){
            String s = plugin.convertPath(RConfigPath.MESSAGE_GAME_MOVE_COUNT).toString();
            s = s.replace("&", "§");
            s = s.replace("%count%", Integer.toString(getCount()));
            
            Bukkit.broadcastMessage(plugin.getPrefix() + s);
        } else if(getCount() == 0){
            stop(false);
			String game_started = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_STARTED).toString();
			game_started = game_started.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + game_started);
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.GAME_GRACE_PERIOD);
            getGameTimer("game_grace").start(1, false);
        }
    }
    
}
