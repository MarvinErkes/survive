/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RActionBar;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RDeathmatchTimer extends RGameTimer
{
	private final Survive plugin;

	public RDeathmatchTimer(Survive instance,String name, int count)
	{
		super(name, count);
		this.plugin = instance;
	}

	@Override
	public boolean performHeader()
	{
		plugin.getGameManager().setCurrentGameStatus(RGameStatus.DEATH_MATCH);
		return true;
	}

	@Override
	public void performBody()
	{
		if(Bukkit.getOnlinePlayers().size() - RGamePlayer.getSpectatorAmount() == 1){

			RGamePlayer win = RGamePlayer.getWinnedPlayer();
			win.addPoints(20);

			if((boolean) plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
				win.insertModulStats(plugin.getVoteManager().getWinnedChallenge().getName(), 1, 1);
			}


			plugin.getBackupManager().allowGameReset(true);
			String game_won = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_WON).toString();
			game_won = game_won.replace("&", "§").replace("%winner%", win.getName());
			Bukkit.broadcastMessage(plugin.getPrefix() + game_won);
			stop(false);
			getGameTimer("break_count").start(1, false);  
			return;

		}
		if(getCount() <= 10 && getCount() > 0){
			String dm_ending = Survive.getInstance().convertPath(RConfigPath.MESSAGE_DM_ENDING).toString();
			dm_ending = dm_ending.replace("&", "§").replace("%count%", String.valueOf(getCount()));
			RActionBar.set(dm_ending);
		}
		if(getCount() == 0){
			String dm_end_no_win = Survive.getInstance().convertPath(RConfigPath.MESSAGE_DM_END_NO_WIN).toString();
			dm_end_no_win = dm_end_no_win.replace("&", "§");
			Bukkit.broadcastMessage(plugin.getPrefix() + dm_end_no_win);
			stop(false);
			plugin.getBackupManager().allowGameReset(true);
			getGameTimer("break_count").start(1, false);
		}
	}

}
