/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.modul.RBasicModul;
import io.rince.survive.util.RGameMap;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RLobbyTimer extends RGameTimer{
    private final Survive plugin;

    public RLobbyTimer(Survive instance,String name, int count) {
        super(name, count);
        this.plugin = instance;
    }

    @Override
    public boolean performHeader() {
        for(RGameMap map : RGameMap.getGameMaps()){
            if(!map.isReady()){
                return false;
            }
        }
        if(RGameMap.getGameMaps().length == 0 || RBasicModul.getChallenges().length == 0){
            return false;
        }
        if(Bukkit.getOnlinePlayers().size() >= (int)plugin.convertPath(RConfigPath.SETTINGS_MIN_PLAYER)){
            if((boolean) plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
                if(RBasicModul.getChallenges().length == 0){
                    Bukkit.broadcastMessage(plugin.getPrefix() + "§eModulList is empty!");
                    return false;
                }
            }
            Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_LOBBY_START).toString().replace("&", "§").replace("%n%", "\n" + plugin.getPrefix()));
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.LOBBY);
            plugin.getVoteManager().setVote(true);
            plugin.getVoteManager().registerRandomMaps();
            plugin.getVoteManager().registerRandomModuls();
            return true;
        }
        
        return false;
    }
    @Override
    public void performBody() {
        int count = getCount();
        
        if(Bukkit.getOnlinePlayers().size() < (int) plugin.convertPath(RConfigPath.SETTINGS_MIN_PLAYER)){
            Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_NOT_ENOTH_PLAYER).toString().replace("&", "$").replace("%n%", "\n" + plugin.getPrefix()));
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.BEFORE_LOBBY);
            RGameMap.resetAllVotes();
            RBasicModul.resetAllVotes();
            plugin.getVoteManager().setVote(false);
            stop(true);
            return;
        }
        
        
        if(count == (int) plugin.convertPath(RConfigPath.SETTINGS_VOTE_TO_COUNT)){
            String win = (String)plugin.convertPath(RConfigPath.MESSAGE_VOTE_WIN);
            win = win.replace("&", "§");
            win = win.replace("%n%", "\n" + plugin.getPrefix());
            win = win.replace("%map%", plugin.getVoteManager().getWinnedMap().getDisplayName().replace("&", "§"));
            Bukkit.broadcastMessage(plugin.getPrefix() + win);
            
            if((boolean) plugin.convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
                String m = (String) plugin.convertPath(RConfigPath.MESSAGE_MODUL_WIN);
                m = m.replace("&", "§");
                m = m.replace("%modul%", plugin.getVoteManager().getWinnedChallenge().getDisplayName().replace("&", "§"));
                Bukkit.broadcastMessage(plugin.getPrefix() + m);
            }
            
            plugin.getVoteManager().setVote(false);
            plugin.getVoteManager().clearBoard();
        } else {
            if(plugin.getVoteManager().canVote()){
                plugin.getVoteManager().showBoard();
            }
        }
        
        if(Integer.toString(count).contains("0") && count > 0 && count != 10){
            String msg = (String) plugin.convertPath(RConfigPath.MESSAGE_LOBBY_COUNT);
            msg = replaceIdent(msg);
            Bukkit.broadcastMessage(plugin.getPrefix() + msg);
        }
        if(count <= 10 && count > 0){
            String msg = (String) plugin.convertPath(RConfigPath.MESSAGE_LOBBY_COUNT);
            msg = replaceIdent(msg);
            Bukkit.broadcastMessage(plugin.getPrefix() + msg);
        }
        if(count == 0){
            stop(false);
            RGameTimer.getGameTimer("game_spawns_timer").start(1, false);
        }
        
    }
    
    
    private String replaceIdent(String s){
        s = s.replace("%count%", Integer.toString(getCount()));
        s = s.replace("&","§");
        s = s.replace("%n%", "\n" + plugin.getPrefix());
        return s;
    }

    
    
}
