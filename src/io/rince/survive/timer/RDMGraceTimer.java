/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RActionBar;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RDMGraceTimer extends RGameTimer
{
    private final Survive plugin;

    public RDMGraceTimer(Survive instance,String name, int count)
    {
        super(name, count);
        this.plugin = instance;
    }

    @Override
    public boolean performHeader()
    {
        plugin.getGameManager().setCurrentGameStatus(RGameStatus.DEATH_MATCH_GRACE_PERIOD);
        plugin.getVoteManager().getWinnedMap().getDeathmatchArena().teleportAll();
        return true;
    }

    @Override
    public void performBody()
    {
        if(Bukkit.getOnlinePlayers().size() - RGamePlayer.getSpectatorAmount() == 1){
			String dm_stop_not_enough_player = Survive.getInstance().convertPath(RConfigPath.MESSAGE_DM_STOP_NOT_ENOUGH_PLAYERS).toString();
			dm_stop_not_enough_player = dm_stop_not_enough_player.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + dm_stop_not_enough_player);
            stop(false);
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.END);
            getGameTimer("break_count").start(1, false);
            plugin.getBackupManager().allowGameReset(true);
            return;
        }
        if(getCount() <= 10 && getCount() > 0){
			String grace_ending = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GRACE_ENDING).toString();
			grace_ending = grace_ending.replace("&", "§").replace("%count%", String.valueOf(getCount()));
            RActionBar.set(grace_ending);
        }
        if(getCount() == 0){
			String game_started = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_STARTED).toString();
			game_started = game_started.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + game_started);
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.DEATH_MATCH);
            stop(false);
            getGameTimer("dm_count").start(1, false);
        }
    }
    
}
