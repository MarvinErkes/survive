/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RActionBar;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RGameGraceTimer extends RGameTimer
{
    private final Survive plugin;

    public RGameGraceTimer(Survive instance,String name, int count)
    {
        super(name, count);
        this.plugin = instance;
    }

    @Override
    public boolean performHeader()
    {
        plugin.getGameManager().setCurrentGameStatus(RGameStatus.GAME_GRACE_PERIOD);
        return true;
    }

    @Override
    public void performBody()
    {
        
        if(Bukkit.getOnlinePlayers().size() < (int) plugin.convertPath(RConfigPath.SETTINGS_MIN_PLAYER)){
			String grace_stop = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GRACE_STOP_NOT_ENOUGH_PLAYERS).toString();
			grace_stop = grace_stop.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + grace_stop);
            getGameTimer("break_count").start(0, false);
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.END);
            stop(false);
        }
        if(getCount() == 0){
			String grace_ended = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GRACE_ENDED).toString();
			grace_ended = grace_ended.replace("&", "§");
            Bukkit.broadcastMessage(plugin.getPrefix() + grace_ended);
            plugin.getGameManager().setCurrentGameStatus(RGameStatus.IN_GAME);
            stop(false);
            getGameTimer("game_count").start(1, false);
        }
        if(getCount() == 50 || getCount() == 40 || getCount() == 30 || getCount() == 20 || getCount() <= 10 && getCount() > 0){
			String grace_ending = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GRACE_ENDING).toString();
			grace_ending = grace_ending.replace("&", "§").replace("%count%", String.valueOf(getCount()));
            RActionBar.set(grace_ending);
        }
    }
    
}
