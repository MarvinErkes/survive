/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RActionBar;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RInGameTimer extends RGameTimer
{
    private final Survive plugin;
    private int vielfaches = 0;
    private boolean start_dm = false;

    public RInGameTimer(Survive instance,String name, int count)
    {
        super(name, count);
        this.plugin = instance;
    }

    @Override
    public boolean performHeader()
    {
        vielfaches = 10;
        plugin.getGameManager().setCurrentGameStatus(RGameStatus.IN_GAME);
        
        return true;
    }

    @Override
    public void performBody()
    {
        int count = getCount();
        if(Bukkit.getOnlinePlayers().size() - RGamePlayer.getSpectatorAmount() == 1){
            
            RGamePlayer win = RGamePlayer.getWinnedPlayer();
            win.addPoints(20);
            win.insertModulStats(plugin.getVoteManager().getWinnedChallenge().getName(), 1, 1);
            
            plugin.getBackupManager().allowGameReset(true);
			String game_won = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_WON).toString();
			game_won = game_won.replace("&", "§").replace("%winner%", win.getName());
            Bukkit.broadcastMessage(plugin.getPrefix() + game_won);
            stop(false);
            getGameTimer("break_count").start(1, false);
            
            return;
        }
        if(count == 60*vielfaches && vielfaches > 0){
			String dm_starting = Survive.getInstance().convertPath(RConfigPath.MESSAGE_DM_STARTING).toString();
			dm_starting = dm_starting.replace("&", "§").replace("%count%", String.valueOf((count/60)));
            RActionBar.set(dm_starting);
            vielfaches--;
        }
        if(Bukkit.getOnlinePlayers().size() - RGamePlayer.getSpectatorAmount() <= (int) plugin.convertPath(RConfigPath.SETTINGS_DEATHMATCH_START_AMOUNT)){
            if(!start_dm){
                setCount(60);
                start_dm = true;
    			String dm_forced = Survive.getInstance().convertPath(RConfigPath.MESSAGE_DM_FORCED).toString();
    			dm_forced = dm_forced.replace("&", "§");
                Bukkit.broadcastMessage(plugin.getPrefix() + dm_forced);
            }
        }
        
        if(count <= 15 && count > 0){
            Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_DEATHMATCH_COUNT).toString().replace("&", "§").replace("%count%", Integer.toString(count)));
        }
        if(count == 0){
            stop(false);
            getGameTimer("dm_grace").start(1, false);
        }
    }
    
}
