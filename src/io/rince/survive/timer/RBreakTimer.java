/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.timer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RBungeeUtil;
import io.rince.survive.util.RGamePlayer;
import io.rince.survive.util.RGameTimer;

/**
 *
 * @author Rince
 */
public class RBreakTimer extends RGameTimer
{
    private final Survive plugin;

    public RBreakTimer(Survive instance,String name, int count)
    {
        super(name, count);
        this.plugin = instance;
    }

    @Override
    public boolean performHeader()
    {
        plugin.getGameManager().setCurrentGameStatus(RGameStatus.END);
        Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_SERVER_RESTART).toString().replace("&", "§").replace("%count%", Integer.toString(getCount())));
        return true;
    }

    @Override
    public void performBody()
    {
        for(RGameTimer timer : getGameTimers()){
            if(!timer.equals(this) && timer.isRunning()){
                timer.stop(false);
            }
        }
        if(getCount() <= 5 && getCount() > 0){
            Bukkit.broadcastMessage(plugin.getPrefix() + plugin.convertPath(RConfigPath.MESSAGE_SERVER_RESTART).toString().replace("&", "§").replace("%count%", Integer.toString(getCount())));
        }
        if(getCount() == 0){
            for(RGamePlayer player : RGamePlayer.getGamePlayers()){
                Bukkit.broadcastMessage("Start Saving.. " + player.getName() + " [" + RGamePlayer.getGamePlayers().length + "]");
                
                if(!player.getName().equalsIgnoreCase(RGamePlayer.getWinnedPlayer().getName())){
                    if(!player.isSpectator()){
                        player.insertModulStats(plugin.getVoteManager().getWinnedChallenge().getName(), 1, 0);
                        Bukkit.broadcastMessage("Break Timer: " + player.getName());
                    }
                }
                player.saveData();
            }
            
            for(Player p : Bukkit.getOnlinePlayers()){
                RBungeeUtil.sendPlayer(p, plugin.convertPath(RConfigPath.SETTINGS_SERVER_FALLBACK).toString());
                String game_ended = Survive.getInstance().convertPath(RConfigPath.MESSAGE_GAME_ENDED).toString();
                game_ended = game_ended.replace("&", "§");
                p.kickPlayer(plugin.getPrefix() + game_ended);
            }
            Bukkit.reload();
        }
    }
    
}
