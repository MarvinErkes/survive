/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.io.File;
import java.io.FileNotFoundException;

import io.rince.survive.Survive;

/**
 *
 * @author Rince
 */
public class RGameBackup
{
    
    private File file;
    private File world;
    private RGameMap map;
    private RDeathmatchArena dm;
    
    public RGameBackup(RGameMap map) throws FileNotFoundException{
        File f = new File(Survive.getInstance().getDataFolder() + "/backups/" + map.getWorld().getName());
        if(!f.isDirectory() && f.exists()){
            throw new FileNotFoundException("GameBackup is not valid!");
        }
        this.file = f;
        this.world = new File(f.getName());
        if(!this.world.exists()){
            throw new FileNotFoundException("World Folder not found!");
        }
        this.map = map;
    }
    public RGameBackup(RDeathmatchArena dm){
        this.world = new File(dm.getWorld().getName());
        this.file = new File(Survive.getInstance().getDataFolder() + "/backups/" + dm.getWorld().getName());
        this.dm = dm;
    }
    
    public RGameMap getGameMap(){
        return map;
    }
    public RDeathmatchArena getDeathmatchArena(){
        return dm;
    }
    public File getBackupFile(){
        return file;
    }
    public File getWorldFolder(){
        return world;
    }
    
    @Override
    public boolean equals(Object ob){
        if(!(ob instanceof RGameBackup)){
            return false;
        }
        RGameBackup b = (RGameBackup) ob;
        if(b.getBackupFile().getPath().equalsIgnoreCase(this.file.getPath()) && b.getWorldFolder().getPath().equalsIgnoreCase(world.getPath())){
            return true;
        }
        
        return false;
    }
    
}
