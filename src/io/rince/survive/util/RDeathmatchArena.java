/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import io.rince.survive.Survive;


/**
 *
 * @author Rince
 */
public class RDeathmatchArena
{
    
    private List<Location> spawns;
    private RGameBackup backup;
    private World world;
    private final Survive plugin;
    
    public RDeathmatchArena(Survive instance,World world){
        spawns = new ArrayList<>();
        this.plugin = instance;
        this.world = world;
        
        if(new File(Survive.getInstance().getDataFolder() + "/backups/" + world.getName()).exists()){
            this.backup = new RGameBackup(this);
        } else {
            this.backup = null;
        }
    }
    
    public void addSpawn(Location loc){
        this.spawns.add(loc);
    }
    public List<Location> getSpanws(){
        return spawns;
    }
    public World getWorld(){
        return world;
    }
    public RGameBackup getBackup(){
        return backup;
    }
    public void setBackup(RGameBackup backup){
        this.backup = backup;
    }
    public void teleportAll(){
        int id = 0;
        for(RGamePlayer player : RGamePlayer.getGamePlayers()){
            if(player.isSpectator()){
                continue;
            }
            Bukkit.getPlayer(player.getUUID()).teleport(spawns.get(id));
            id++;
            
            if(id >= spawns.size()){
                for(RGameTimer timer : RGameTimer.getGameTimers()){
                    timer.stop(false);
                }
                Bukkit.broadcastMessage(plugin.getPrefix() + "Es wurde nicht genügend Spawns für das Deathmatch gesetzt!");
                RGameTimer.getGameTimer("break_count").start(1, false);
                break;
            }
        }
    }
    
}
