/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;

/**
 *
 * @author Rince
 */
public class RBlockList
{
    
    private List<Material> list;
    
    public RBlockList(){
        list = new ArrayList<>();
    }
    
    public void addBlock(Material m){
        if(m.isBlock()){
            list.add(m);
        }
    }
    public boolean containsBlock(Material m){
        return list.contains(m);
    }
    public void removeBlock(Material m){
        if(containsBlock(m)){
            list.remove(m);
        }
    }
    public Material[] getBlocks(){
        Material[] list = new Material[this.list.size()];
        return this.list.toArray(list);
    }
}
