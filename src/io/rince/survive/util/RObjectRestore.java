/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.util.HashMap;

/**
 *
 * @author Rince
 */
public class RObjectRestore
{
    private HashMap<String,Object> objects;
    
    public RObjectRestore(){
        objects = new HashMap<>();
    }
    
    public void addObject(String name,Object ob){
        objects.put(name, ob);
    }
    public Object getObject(String name){
        return objects.get(name);
    }
    @SuppressWarnings("unchecked")
	public HashMap<String,Object> getObjects(){
        return (HashMap<String, Object>) objects.clone();
    }
}
