package io.rince.survive.util;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class RTitle
{
  public static void set(Player arg0, int arg1, int arg2, int arg3, String header, String footer)
  {
    PacketPlayOutTitle times = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, arg1, arg2, arg3);
    ((CraftPlayer)arg0).getHandle().playerConnection.sendPacket(times);
    if (header != null) {
      ((CraftPlayer)arg0).getHandle().playerConnection.sendPacket(new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}")));
    }
    if (footer != null) {
      ((CraftPlayer)arg0).getHandle().playerConnection.sendPacket(new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}")));
    }
  }
  
  public static void set(int arg0, int arg1, int arg2, String header, String footer)
  {
    for (Player p : Bukkit.getOnlinePlayers()) {
      set(p, arg0, arg1, arg2, header, footer);
    }
  }
}
