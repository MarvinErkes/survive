/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;
import io.rince.survive.enums.RModulStatsType;
import io.rince.survive.enums.RStatsType;
import io.rince.survive.manager.RDatabaseConnection;
import io.rince.survive.manager.RStatsConnection;

/**
 *
 * @author Rince
 */
public class RGamePlayer {
    
    private int kills,deaths,points;
    private UUID uuid;
    private String name;
    private boolean spectator;
    private List<RObjectRestore> modul_stats;
    
    private static List<RGamePlayer> players = new ArrayList<RGamePlayer>();
    
    public RGamePlayer(String name,UUID player,boolean spectator)
    {
        this.name = name;
        this.uuid = player;
        this.spectator = spectator;
        if(!this.spectator){
            fetchData();
        }
        
        players.add(this);
    }
    
    public int getKills()
    {
        return kills;
    }
    public int getDeaths()
    {
        return deaths;
    }
    public int getPoints()
    {
        return points;
    }
    public String getName()
    {
        return name;
    }
    public UUID getUUID()
    {
        return uuid;
    }
    public List<RObjectRestore> getModulStats(){
        return modul_stats;
    }
    public int calculateModulStats(RModulStatsType type){
        int i = 0;
        for(RObjectRestore r : getModulStats()){
            i+= (int)r.getObject(type.getRestoreName());
        }
        return i;
    }
    public boolean isSpectator(){
        return spectator;
    }
    public void setSpectator(boolean b){
        this.spectator = b;
    }
    
    public void addKill()
    {
        kills++;
    }
    public void insertModulStats(String modul_name,int games,int wins){
        if(modul_name == null){
            return;
        }
        if(this.modul_stats != null){
            for(RObjectRestore restore : modul_stats){
                if(restore.getObject("name").toString().equalsIgnoreCase(modul_name)){
                    
                    //Bukkit.broadcastMessage("Found and edit [" + modul_name + ";" + this.name + "]");
                    
                    restore.addObject("wins", (int)restore.getObject("wins") + wins);
                    restore.addObject("games", (int) restore.getObject("games") + games);
                    
                    Bukkit.broadcastMessage("size: " + restore.getObjects().size());
                    return;
                }
            }
            
            //Bukkit.broadcastMessage("Not found and create [" + modul_name + ";" + this.name + "]");
            RObjectRestore r = new RObjectRestore();
            r.addObject("name", modul_name);
            r.addObject("wins", wins);
            r.addObject("games", games);
            this.modul_stats.add(r);
            
        } else {
            this.modul_stats = new ArrayList<>();
            RObjectRestore r = new RObjectRestore();
            r.addObject("name", modul_name);
            r.addObject("wins", wins);
            r.addObject("games", games);
            
            this.modul_stats.add(r);
        }
    }
    public void addPoints(int points)
    {
        this.points+= points;
    }
    public void addDeath()
    {
        deaths++;
    }
    
    
    public void fetchData()
    {
        Survive c = Survive.getInstance();
        RStatsConnection stats = c.getStatsConnection();
        stats.createStatsSelection(uuid);
        if(c.getConnection().hasConnection()){
            this.kills = stats.getStats(uuid, RStatsType.KILL);
            this.deaths = stats.getStats(uuid, RStatsType.DEATH);
            this.points = stats.getStats(uuid, RStatsType.POINT);
            this.modul_stats = stats.getPlayerModuls(uuid);
        }
    }
    
    public void saveData()
    {
        RDatabaseConnection con = Survive.getInstance().getConnection();
        con.queryUpdate("UPDATE s_stats SET kills=" + kills + ", deaths=" + deaths + ", points=" + points + " WHERE player='" + uuid.toString() + "'");
        
        
        if(!(boolean) Survive.getInstance().convertPath(RConfigPath.SETTINGS_ENABLE_MODULS)){
            Bukkit.broadcastMessage("Moduls offline!");
            return;
        }
        if(modul_stats == null){
            Bukkit.broadcastMessage("Modul stats null!");
            return;
        }
        for(RObjectRestore restore : modul_stats){
            
            //Bukkit.broadcastMessage("LOOP [" + restore.getObject("name").toString() + ";" + this.name + "]");
            
            int games = (int)restore.getObject("games");
            int wins = (int)restore.getObject("wins");
            String name = restore.getObject("name").toString();
            if(Survive.getInstance().getStatsConnection().hasPlayedModul(uuid, name)){
                con.queryUpdate("UPDATE modul_stats SET wins=" + wins + ", games=" + games + " WHERE player='" + uuid.toString() + "' AND modul='" + name + "'");
            } else {
                con.queryUpdate("INSERT INTO modul_stats(player,modul,games,wins) VALUES ('" + uuid.toString() + "','" + name + "'," + games + "," + wins + ")");
            }
        }
    }
    
    
    public static RGamePlayer getGamePlayer(String name){
        for(RGamePlayer p : players){
            if(p.getName().equalsIgnoreCase(name)){
                return p;
            }
        }
        return null;
    }
    public static RGamePlayer getGamePlayer(UUID uuid){
        for(RGamePlayer p : players){
            if(p.getUUID().equals(uuid)){
                return p;
            }
        }
        return null;
    }
    public static RGamePlayer[] getGamePlayers(){
        RGamePlayer[] list = new RGamePlayer[players.size()];
        return players.toArray(list);
    }
    public static int getSpectatorAmount(){
        int i = 0;
        for(RGamePlayer player : players){
            if(player.isSpectator()){
                i++;
            }
        }
        return i;
    }
    public static RGamePlayer getWinnedPlayer(){
        if(Bukkit.getOnlinePlayers().size() - getSpectatorAmount() == 1){
            for(RGamePlayer p : players){
                if(!p.isSpectator()){
                    return p;
                }
            }
        }
        return null;
    }
    public static void removeGamePlayer(UUID player,boolean save_data){
        RGamePlayer gp = getGamePlayer(player);
        if(gp != null){
            if(save_data){
                gp.saveData();
            }
            players.remove(gp);
        }
    }
    
}
