package io.rince.survive.util;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class RActionBar
{
  public static void set(Player arg0, String arg1)
  {
    ((CraftPlayer)arg0).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + arg1 + "\"}"), (byte)2));
  }
  
  public static void set(String arg0)
  {
    for (Player p : Bukkit.getOnlinePlayers()) {
      set(p, arg0);
    }
  }
}
