/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.entity.Player;

import io.rince.survive.Survive;

/**
 *
 * @author Rince
 */
public class RBungeeUtil
{
    
    public static void sendPlayer(Player p,String server){
        try
        {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream stream = new DataOutputStream(b);
            
            stream.writeUTF("Connect");
            stream.writeUTF(server);
            
            p.sendPluginMessage(Survive.getInstance(), server,b.toByteArray());
        } catch (IOException ex)
        {
            Logger.getLogger(RBungeeUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
