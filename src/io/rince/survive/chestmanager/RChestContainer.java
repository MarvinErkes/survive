/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.chestmanager;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.inventory.ItemStack;

public class RChestContainer 
{
    
    private List<ItemStack> container;
    
    public RChestContainer(){
        container = new ArrayList<>();
    }
    public RChestContainer(List<ItemStack> container){
        this.container = container;
    }
    
    public void addItemStack(ItemStack i){
        this.container.add(i);
    }
    public void removeItemStack(ItemStack i){
        if(container.contains(i)){
            container.remove(i);
        }
    }
    public List<ItemStack> getContainer(){
        return container;
    }
    
}
