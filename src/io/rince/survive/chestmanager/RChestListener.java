/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.chestmanager;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import io.rince.survive.Survive;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.util.RGamePlayer;

/**
 *
 * @author Rince
 */
public class RChestListener implements Listener
{
    
    public RChestListener(){
        Survive.getInstance().getServer().getPluginManager().registerEvents(this, Survive.getInstance());
    }
    
    @SuppressWarnings("deprecation")
	@EventHandler
    public void interact(PlayerInteractEvent e){
        RGameStatus status = Survive.getInstance().getGameManager().getCurrentGameStatus();
        if(status == RGameStatus.BEFORE_LOBBY || status == RGameStatus.END || status == RGameStatus.GAME_ON_SPAWNS || status == RGameStatus.LOBBY){
            return;
        }
        
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
            Block b = e.getClickedBlock();
            if(b.getType() == Material.getMaterial(Survive.getInstance().getChestManager().getBlockID())){
                if(Survive.getInstance().getChestManager().getBlockSubID() != 0){
                    if(b.getData() != Survive.getInstance().getChestManager().getBlockSubID()){
                        e.setCancelled(true);
                        return;
                    }
                }
                RGamePlayer player = RGamePlayer.getGamePlayer(e.getPlayer().getUniqueId());
                if(player.isSpectator()){
                    e.setCancelled(true);
                    return;
                }
                
                if(Survive.getInstance().getChestManager().containsChest(b.getLocation())){
                    e.setCancelled(true);
                    e.getPlayer().openInventory(Survive.getInstance().getChestManager().createChest(b.getLocation()));
                } else {
                    e.setCancelled(true);
                    e.getPlayer().openInventory(Survive.getInstance().getChestManager().createChest(b.getLocation()));
                }
            }
        }
    }
    
}
