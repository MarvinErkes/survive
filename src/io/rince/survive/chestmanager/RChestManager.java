/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.chestmanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import io.rince.survive.Survive;
import io.rince.survive.enums.RConfigPath;

/**
 *
 * @author Rince
 */
public class RChestManager
{
    
    private List<ItemStack> content;
    private HashMap<Location,Inventory> chests;
    private Random r;
    private final Survive plugin;
    @SuppressWarnings("unused")
	private int task;
    
    public RChestManager(Survive instance){
        this.chests = new HashMap<>();
        this.content = new ArrayList<>();
        r = new Random();
        this.plugin = instance;
    }
    
    public void addContainer(RChestContainer container){
        for(ItemStack i : container.getContainer()){
            content.add(i);
        }
    }
    public Inventory createChest(Location loc){
        if(chests.containsKey(loc)){
            return chests.get(loc);
        }
        Inventory inv = Bukkit.createInventory(null, (int) plugin.convertPath(RConfigPath.CHEST_ROWS)*9, (String) plugin.convertPath(RConfigPath.CHEST_NAME).toString().replace("&", "§"));
        int items = r.nextInt((int)plugin.convertPath(RConfigPath.CHEST_MAX_ITEMS))+1;
        while(items != 0){
            items--;
            int pos = r.nextInt(inv.getSize());
            
            inv.setItem(pos, content.get(r.nextInt(content.size())));
        }
        
        this.chests.put(loc, inv);
        return inv;
    }
    public Inventory getChest(Location loc){
        return chests.get(loc);
    }
    public boolean containsChest(Location loc){
        return chests.containsKey(loc);
    }
    public int getBlockID(){
        String s = plugin.convertPath(RConfigPath.CHEST_BLOCK).toString();
        if(s.contains(":")){
            String[] values = s.split(":");
            return Integer.valueOf(values[0]);
        }
        return Integer.valueOf(s);
    }
    public byte getBlockSubID(){
        String s = plugin.convertPath(RConfigPath.CHEST_BLOCK).toString();
        if(s.contains(":")){
            String[] values = s.split(":");
            return Byte.valueOf(values[1]);
        }
        return 0;
    }
    public void startRefill(final int refill_time){
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable()
        {

            @Override
            public void run()
            {
                if(plugin.getVoteManager().getWinnedMap().getWorld().getTime() >= refill_time){
                    plugin.getVoteManager().getWinnedMap().getWorld().setTime(0);
                    String chest_refill = Survive.getInstance().convertPath(RConfigPath.MESSAGE_CHEST_REFILL).toString();
                    chest_refill = chest_refill.replace("&", "§");
                    Bukkit.broadcastMessage(plugin.getPrefix() + chest_refill);
                    chests.clear();
                }
            }
        },20,20);
    }
}
