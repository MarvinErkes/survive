/*

 * Rince takes the waiting out of wanting. 

 */

package io.rince.survive.modul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import io.rince.survive.Survive;
import io.rince.survive.enums.RGameStatus;
import io.rince.survive.events.SModulLoadEvent;

/**
 *
 * @author Rince
 */
public class RBasicModul
{
    private String name,display,description;
    private int id;
    private int votes;
    private Plugin plugin;
    private List<Listener> listener;
    private ItemStack vote_item;
    private boolean isvote;
    
    private static List<RBasicModul> challanges = new ArrayList<RBasicModul>();
    private static List<String> vote_list = new ArrayList<String>();
    
    public RBasicModul(Plugin plugin,String name,String displayname,String description,ItemStack vote_item)
    {
        this.name = name;
        this.display = displayname;
        this.id = challanges.size()+1;
        this.votes = 0;
        this.plugin = plugin;
        listener = new ArrayList<>();
        this.vote_item = vote_item;
        this.description = description;
        this.isvote = false;
        
        SModulLoadEvent event = new SModulLoadEvent(name);
        Bukkit.getPluginManager().callEvent(event);
        if(!event.isCancelled()){
            for(Player all : Bukkit.getOnlinePlayers()){
                if(!all.isOp()){
                    continue;
                }
                all.sendMessage(Survive.getInstance().getPrefix() + "Ein Modul wurde hinzugefügt [\"§e" + name + "§7\"]");
            }
        } else {
            Bukkit.getPluginManager().disablePlugin(plugin);
            return;
        }
        
        challanges.add(this);
    }
    
    public String getName()
    {
        return name;
    }
    public String getDisplayName()
    {
        return display;
    }
    public String getDescription(){
        return description;
    }
    public boolean isVoteModul(){
        return isvote;
    }
    public int getVotes(){
        return votes;
    }
    public int getID(){
        return id;
    }
    public Plugin getPlugin(){
        return plugin;
    }
    public ItemStack getVoteItem(){
        return vote_item;
    }
    
    public void addListener(Listener listener){
        this.listener.add(listener);
    }
    
    @Deprecated
    public void setVoteModul(boolean b){
        if(Survive.getInstance().getGameManager().getCurrentGameStatus() == RGameStatus.LOBBY){
            this.isvote = b;
        }
    }
    
    @Deprecated
    public void activateListener(){
        for(Listener l : listener){
            Bukkit.getPluginManager().registerEvents(l, Survive.getInstance());
        }
    }
    public void addVote(String p_name){
        if(!vote_list.contains(p_name)){
            vote_list.add(p_name);
            this.votes++;
        }
    }
    public void resetVotes(){
        this.votes = 0;
    }
    
    public static RBasicModul getChallenge(String name){
        RBasicModul c = null;
        for(RBasicModul ch : challanges){
            if(ch.getName().equalsIgnoreCase(name)){
                c = ch;
                break;
            }
        }
        return c;
    }
    public static RBasicModul getByPluginName(String name){
        for(RBasicModul m : challanges){
            if(m.getPlugin().getName().equalsIgnoreCase(name)){
                return m;
            }
        }
        return null;
    }
    public static RBasicModul getByDisplayName(String dname,boolean use_color){
        for(RBasicModul modul : challanges){
            String d = modul.getDisplayName();
            if(use_color){
                d = d.replace("&", "§");
                dname = dname.replace("&", "§");
            } else {
                d = ChatColor.stripColor(d.replace("&", "§"));
                dname = ChatColor.stripColor(dname.replace("&", "§"));
            }
            if(d.equalsIgnoreCase(dname)){
                return modul;
            }
        }
        return null;
    }
    public static RBasicModul[] getChallenges(){
        RBasicModul[] list = new RBasicModul[challanges.size()];
        return challanges.toArray(list);
    }
    public static RBasicModul getRandomModul(){
        return challanges.get(new Random().nextInt(challanges.size()));
    }
    public static boolean hasAlreadyVoted(String name){
        return vote_list.contains(name);
    }
    public static void resetAllVotes(){
        for(RBasicModul c : challanges){
            c.resetVotes();
        }
        vote_list.clear();
    }
}
